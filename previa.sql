-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 17/10/2017 às 14:19
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `talencoimoveis`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` text COLLATE utf8_unicode_ci NOT NULL,
  `legenda_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_do_empreendimento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `link`, `subtitulo`, `legenda_1`, `nome_do_empreendimento`, `titulo`, `area`, `valor`, `legenda_2`, `created_at`, `updated_at`) VALUES
(1, 0, '47_20171017043236.jpg', '#', 'APARTAMENTO<br />\r\nPRONTO PARA MORAR', 'Entregue em: Abril 2017', 'NOME DO EMPREENDIMENTO', 'CAMPO BELO', '58 e 76m²', 'R$ 350.000,00', '*Ref unid. 15 - 1º andar - SET/2017', '2017-10-17 04:32:37', '2017-10-17 04:32:37');

-- --------------------------------------------------------

--
-- Estrutura para tabela `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `blog`
--

INSERT INTO `blog` (`id`, `blog_categoria_id`, `slug`, `data`, `titulo`, `capa`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'exemplo-de-post', '2017-10-17', 'Exemplo de Post', '70_20171017043723.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu quam eget tortor luctus dapibus. Integer convallis congue tellus, imperdiet sodales quam sodales cursus. Cras ut tempor ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et tortor facilisis arcu euismod mattis. Phasellus tincidunt sed nunc nec laoreet. Maecenas lectus orci, dignissim vitae orci sit amet, vehicula egestas dui. Vestibulum vitae elit fermentum, molestie tellus eu, egestas tortor. Maecenas ac enim justo. Nulla facilisi. Maecenas pretium congue nunc ut vulputate. In hac habitasse platea dictumst.</p>\r\n\r\n<p><img src=\"http://talencoimoveis.dev/assets/img/editor/66_20171017043721.jpg\" /></p>\r\n\r\n<p>Suspendisse mattis velit eu finibus venenatis. Aenean cursus lacinia commodo. Mauris tincidunt, lacus non luctus viverra, augue elit consequat ex, et condimentum dolor turpis ut risus. Maecenas fringilla non elit quis mattis. Aliquam dapibus dictum nisi. Donec placerat, leo nec pharetra tincidunt, mauris libero feugiat sapien, id dictum neque turpis a nunc. Aenean porttitor turpis ac augue interdum, vel fringilla nibh ultricies. Vivamus non dolor eu magna tristique pretium nec ac tellus. Donec nulla diam, sollicitudin ut odio ac, sodales interdum nunc. Sed sapien felis, egestas ut venenatis ac, porttitor non tellus. Nulla et fermentum turpis, efficitur egestas eros. Donec vitae congue justo. Curabitur sodales leo luctus viverra convallis. Vestibulum et dui congue, auctor sem non, condimentum quam. In pulvinar mauris vitae fermentum lacinia.</p>\r\n\r\n<p>Phasellus dapibus facilisis nisi a commodo. Etiam fermentum et nulla sagittis feugiat. Morbi at nibh a magna fringilla vulputate. Praesent ac nisi ornare, ullamcorper sem at, pellentesque augue. Curabitur vel nibh ut diam congue vestibulum. Donec lectus urna, aliquam vel ante at, viverra ornare quam. Aliquam scelerisque vestibulum mauris, vitae auctor eros pulvinar et. Mauris pellentesque ligula vel odio scelerisque hendrerit. Nullam vitae ipsum suscipit nisi ullamcorper cursus a at est. Vivamus facilisis felis elementum eros porta rutrum. In vestibulum eget magna in iaculis. Nunc fermentum urna vel rhoncus vehicula. Nulla nec placerat nisi. Pellentesque ultrices tempus tempor. Donec eget nulla vehicula, bibendum neque sed, posuere libero. Nulla vel odio diam.</p>\r\n', '2017-10-17 04:37:24', '2017-10-17 04:37:24');

-- --------------------------------------------------------

--
-- Estrutura para tabela `blog_categorias`
--

CREATE TABLE `blog_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `blog_categorias`
--

INSERT INTO `blog_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'ARQUITETURA E INTERIORES', 'arquitetura-e-interiores', '2017-10-17 04:36:47', '2017-10-17 04:36:47');

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome_do_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `nome_do_site`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'TalenCo Imóveis', 'TalenCo &middot; Negócios Imobiliários', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `telefone`, `whatsapp`, `email`, `endereco`, `google_maps`, `facebook`, `twitter`, `instagram`, `pinterest`, `youtube`, `created_at`, `updated_at`) VALUES
(1, '13 3395 0005', '13 93395 0005', 'contato@trupe.net', 'Rua do Endereço Completo, 123 &middot; Vila do Bairro &middot; São Paulo, SP &middot; 01234-567', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.145831367559!2d-46.65644434884712!3d-23.563205367471678!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c8da0aa315%3A0xd59f9431f2c9776a!2sAv.+Paulista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1507303225288\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '#facebook', '#twitter', '#instagram', '#pinterest', '#youtube', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_imovel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `curriculos_recebidos`
--

CREATE TABLE `curriculos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curriculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa`
--

CREATE TABLE `empresa` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `unidades_de_negocio` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_construcao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_construcao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_reformas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_reformas` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_negocios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_negocios` text COLLATE utf8_unicode_ci NOT NULL,
  `responsabilidade_social` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `empresa`
--

INSERT INTO `empresa` (`id`, `codigo_video`, `texto_1`, `texto_2`, `unidades_de_negocio`, `imagem_construcao`, `texto_construcao`, `imagem_reformas`, `texto_reformas`, `imagem_negocios`, `texto_negocios`, `responsabilidade_social`, `created_at`, `updated_at`) VALUES
(1, 'gEOo82YldX4', 'SOMOS UMA EMPRESA<br />\r\nORIENTADA A PROCESSOS<br />\r\n<strong>FOCO EM EXCEL&Ecirc;NCIA E ATENDIMENTO PERSONALIZADO</strong>', '<em>CUIDAMOS</em><br />\r\n<strong>DE TODOS OS DETALHES</strong><br />\r\nPARA QUE VOC&Ecirc; SE SINTA REALIZADO E COM TOTAL<br />\r\nSEGURAN&Ccedil;A NA REALIZA&Ccedil;&Atilde;O DO SEU NEG&Oacute;CIO', 'A TALENCO é uma empresa que oferece uma solução completa quando o assunto é a busca do seu imóvel ideal, estruturando-se em três unidades de negócio:', '6_20171017043522.jpg', 'Construção & Incorporação de imóveis e conceitos', '3_20171017043522.jpg', 'Soluções em reformas com a elaboração do projeto arquitetônico, gerenciamento e execução de obras', '21_20171017043522.jpg', 'Intermediação de compra e venda de imóveis diferenciados', 'A TALENCO promove o projeto social LAR BEM ESTAR que busca melhorar a qualidade de vida de pessoas. (projeto em desenvolvimento)', NULL, '2017-10-17 04:35:42');

-- --------------------------------------------------------

--
-- Estrutura para tabela `imoveis_recebidos`
--

CREATE TABLE `imoveis_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_residencial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_celular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_proprietario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logradouro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `negocio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_imovel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_condominio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_iptu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_util` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_total` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dormitorios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suites` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banheiros` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vagas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_vagas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano_construcao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `elevadores` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lazer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `escritura_definitiva` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `financiado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2017_10_04_140641_create_banners_table', 1),
('2017_10_04_141101_create_empresa_table', 1),
('2017_10_04_142818_create_curriculos_recebidos_table', 1),
('2017_10_04_184935_create_imoveis_recebidos_table', 1),
('2017_10_10_141021_create_blog_table', 1),
('2017_10_10_143516_create_newsletter_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `acesso` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `acesso`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$vWyBU3dsHONuVeG60cNsdu3Lz4qSqJc1FoMq50FQYJku0ACUAmZjq', 1, 'lNwPSodQZmDzcZ1Gn4bjy74QLiVFQPJbvLv7FshBEucb1Qef37ewNnzgL6iC', NULL, '2017-10-17 04:31:15');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_blog_categoria_id_foreign` (`blog_categoria_id`);

--
-- Índices de tabela `blog_categorias`
--
ALTER TABLE `blog_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `curriculos_recebidos`
--
ALTER TABLE `curriculos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `imoveis_recebidos`
--
ALTER TABLE `imoveis_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_email_unique` (`email`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `blog_categorias`
--
ALTER TABLE `blog_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `curriculos_recebidos`
--
ALTER TABLE `curriculos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `imoveis_recebidos`
--
ALTER TABLE `imoveis_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_blog_categoria_id_foreign` FOREIGN KEY (`blog_categoria_id`) REFERENCES `blog_categorias` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
