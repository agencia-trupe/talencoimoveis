-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 23/10/2017 às 13:11
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `talencoimoveis`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `destaques`
--

CREATE TABLE `destaques` (
  `id` int(10) UNSIGNED NOT NULL,
  `prontos_para_morar_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prontos_para_morar_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prontos_para_morar_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `em_construcao_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `em_construcao_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `em_construcao_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lancamentos_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lancamentos_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lancamentos_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `destaques`
--

INSERT INTO `destaques` (`id`, `prontos_para_morar_1`, `prontos_para_morar_2`, `prontos_para_morar_3`, `em_construcao_1`, `em_construcao_2`, `em_construcao_3`, `lancamentos_1`, `lancamentos_2`, `lancamentos_3`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `trabalhe_conosco`
--

CREATE TABLE `trabalhe_conosco` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `trabalhe_conosco`
--

INSERT INTO `trabalhe_conosco` (`id`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Venha crescer com a TALENCO!', '<p>A TALENCO IM&Oacute;VEIS est&aacute; contratando consultores imobili&aacute;rios para o seu processo de expans&atilde;o no litoral sul.</p>\r\n\r\n<p>Oferecemos diversos benef&iacute;cios para nossa equipe de vendas.</p>\r\n', NULL, '2017-10-23 13:09:40');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `destaques`
--
ALTER TABLE `destaques`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `trabalhe_conosco`
--
ALTER TABLE `trabalhe_conosco`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `destaques`
--
ALTER TABLE `destaques`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `trabalhe_conosco`
--
ALTER TABLE `trabalhe_conosco`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
