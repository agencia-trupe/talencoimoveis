<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class Tools
{

    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' ' . substr(strtoupper($meses[(int) $mes - 1]), 0, 3) . ' ' . $ano;
    }

    public static function fetchImoveis()
    {
        if (Cache::has('imoveis')) {

            return Cache::get('imoveis');

        } else {

            $url = "http://www.valuegaia.com.br/integra/midia.ashx?midia=GaiaWebServiceImovel&p=494dJdwBWlwoJetmMXqzcntV%2bmwq%2bY9nlKeTxGgPKwMoJetmMXqzcntV%2bmwq%2bY9n2bNxzqu12rw%3d";

            $data  = file_get_contents($url);
            $xml   = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
            $json  = json_encode($xml);
            $array = json_decode($json, true);

            $collection  = collect($array['Imoveis']['Imovel']);
            $lancamentos = collect($array['Lancamentos']['Lancamento']);

            $caracteristicas       = [];
            $caracteristicasCampos = ['piscina', 'piscina_infantil', 'salao_festas', 'salao_jogos', 'churrasqueira', 'gourmet', 'brinquedoteca', 'playground', 'sala_video_cinema', 'sauna_umida', 'sala_fitness'];

            foreach ($lancamentos as $lancamento) {
                if (array_key_exists('ImoveisNoEmpreedimento', $lancamento) && array_key_exists('Imovel', $lancamento['ImoveisNoEmpreedimento'])) {
                    if (!is_array($lancamento['ImoveisNoEmpreedimento']['Imovel'])) {
                        $imoveis = $lancamento['ImoveisNoEmpreedimento'];
                    } else {
                        $imoveis = $lancamento['ImoveisNoEmpreedimento']['Imovel'];
                    }

                    foreach ($imoveis as $key => $codigo) {
                        $imovel = $collection->where('CodigoImovel', $codigo)->first();
                        if ($imovel) {
                            foreach ($caracteristicasCampos as $caracteristica) {
                                if (array_key_exists($caracteristica, $lancamento['DetalhesEmpreendimento'])) {
                                    $caracteristicas[$codigo][$caracteristica] = $lancamento['DetalhesEmpreendimento'][$caracteristica];
                                }
                            }
                        }
                    }
                }
            }

            $collection = $collection->map(function ($imovel) use ($caracteristicas) {
                if (array_key_exists($imovel['CodigoImovel'], $caracteristicas)) {
                    foreach($caracteristicas[$imovel['CodigoImovel']] as $key => $value) {
                        $imovel[$key] = $value;
                    }
                }

                return $imovel;
            });

            Cache::put('imoveis', $collection, Carbon::now()->addMinutes(60));

            return $collection;

        }
    }

    public static function imoveisUniqueOptions($key, $collection = null)
    {
        try {
            if (!$collection) $collection = Tools::fetchImoveis();
            $imoveis = $collection->map(function ($imovel) use ($key) {
                return $imovel[$key];
            })->unique()->toArray();

            $imoveis = array_values($imoveis);
            return array_combine(range(1, count($imoveis)), $imoveis);
        } catch (\Exception $e) {
            return [];
        }
    }

    public static function imoveisBairrosPorCidade()
    {
        $cidades = Tools::imoveisUniqueOptions('Cidade');
        $bairros = [];

        foreach($cidades as $key => $value) {
            $collection = Tools::fetchImoveis()->where('Cidade', $value);
            $bairrosDisponiveis = Tools::imoveisUniqueOptions('Bairro', $collection);

            $bairros[$value] = $bairrosDisponiveis;
        }

        return $bairros;
    }

    public static function chamadaPost($texto)
    {
        $input = strip_tags($texto);

        if (strlen($input) > 200) {
            $pos   = strpos($input, ' ', 200);
            $input = substr($input, 0, $pos) . '...';
        }

        return $input;
    }

    public static function imovelLocacao($imovel) {
        return array_key_exists('TipoLocacao', $imovel);
    }

    public static function imovelVenda($imovel) {
        return !self::imovelLocacao($imovel);
    }

    public static function exibeTipoOferta($imovel) {
        return self::imovelLocacao($imovel) ? 'LOCAÇÃO' : 'VENDA';
    }
}
