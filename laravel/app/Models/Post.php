<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Carbon\Carbon;

class Post extends Model
{
    protected $table = 'blog';

    protected $guarded = ['id'];

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('blog_categoria_id', $categoria_id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopePublicados($query)
    {
        return $query->whereDate('data', '<=', Carbon::today()->toDateString());
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo', 'LIKE', "%{$termo}%")
            ->orWhere('texto', 'LIKE', "%{$termo}%");
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\PostCategoria', 'blog_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 540,
                'height' => 312,
                'path'   => 'assets/img/blog/'
            ],
            [
                'width'  => 800,
                'height' => 465,
                'path'   => 'assets/img/blog/destaque/'
            ],
            [
                'width'  => 340,
                'height' => 196,
                'path'   => 'assets/img/blog/mais/'
            ]
        ]);
    }
}
