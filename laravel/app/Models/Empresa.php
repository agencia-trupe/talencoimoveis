<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_imagem_construcao()
    {
        return CropImage::make('imagem_construcao', [
            'width'  => 510,
            'height' => 410,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_reformas()
    {
        return CropImage::make('imagem_reformas', [
            'width'  => 510,
            'height' => 410,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_negocios()
    {
        return CropImage::make('imagem_negocios', [
            'width'  => 510,
            'height' => 410,
            'path'   => 'assets/img/empresa/'
        ]);
    }
}
