<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Destaques extends Model
{
    protected $table = 'destaques';

    protected $guarded = ['id'];

}
