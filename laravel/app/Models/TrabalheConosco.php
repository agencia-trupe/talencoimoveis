<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class TrabalheConosco extends Model
{
    protected $table = 'trabalhe_conosco';

    protected $guarded = ['id'];

}
