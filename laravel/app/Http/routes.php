<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('imoveis', 'ImoveisController@index')->name('imoveis');
    Route::get('imoveis/{codigo_imovel}', 'ImoveisController@show')->name('imoveis.show');
    Route::get('anuncie-seu-imovel', 'AnuncieController@index')->name('anuncie');
    Route::post('anuncie-seu-imovel', 'AnuncieController@post')->name('anuncie.post');
    Route::get('espaco-talenco/{categoria_slug?}', 'BlogController@index')->name('blog');
    Route::get('espaco-talenco/{categoria_slug}/{post_slug}', 'BlogController@show')->name('blog.show');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('a-talenco', 'EmpresaController@index')->name('empresa');
    Route::get('trabalhe-conosco', 'TrabalheConoscoController@index')->name('trabalhe');
    Route::post('trabalhe-conosco', 'TrabalheConoscoController@post')->name('trabalhe.post');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Retorna JSON com os 3 últimos posts do blog
    Route::get('posts-json', function() {
        return \App\Models\Post::with('categoria')->publicados()->ordenados()->take(3)->get();
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');
		Route::resource('blog/categorias', 'BlogCategoriasController', ['parameters' => ['categorias' => 'categorias_blog']]);
		Route::resource('blog', 'BlogController');

        Route::group(['middleware' => 'acessoCompleto'], function() {
            /* GENERATED ROUTES */
		Route::resource('trabalhe-conosco', 'TrabalheConoscoController', ['only' => ['index', 'update']]);
            Route::resource('destaques', 'DestaquesController', ['only' => ['index', 'update']]);
    		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
    		Route::resource('banners', 'BannersController');
            Route::get('curriculos/{curriculos}/toggle', ['as' => 'painel.curriculos.toggle', 'uses' => 'CurriculosController@toggle']);
            Route::resource('curriculos', 'CurriculosController');
            Route::get('imoveis/{imoveis}/toggle', ['as' => 'painel.imoveis.toggle', 'uses' => 'ImoveisController@toggle']);
            Route::resource('imoveis', 'ImoveisController');
    		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

            Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
            Route::resource('contato/recebidos', 'ContatosRecebidosController');
            Route::resource('contato', 'ContatoController');
            Route::resource('usuarios', 'UsuariosController');
            Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
            Route::resource('newsletter', 'NewsletterController');

            Route::post('ckeditor-upload', 'PainelController@imageUpload');
            Route::post('order', 'PainelController@order');
            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

            Route::get('generator', 'GeneratorController@index')->name('generator.index');
            Route::post('generator', 'GeneratorController@submit')->name('generator.submit');

            Route::get('atualizar-imoveis', function() {
                Cache::forget('imoveis');
                Tools::fetchImoveis();
                return redirect()->route('painel')->with('success', 'Imóveis atualizados com sucesso.');
            })->name('atualizar-imoveis');
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
