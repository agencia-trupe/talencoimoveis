<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Helpers\Tools;

class ImoveisController extends Controller
{
    private function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public $caracteristicas = [
        'piscina'           => 'piscina',
        'piscina_infantil'  => 'piscina infantil',
        'salao_festas'      => 'salão de festas',
        'salao_jogos'       => 'salão de jogos',
        'churrasqueira'     => 'churrasqueira',
        'gourmet'           => 'espaço gourmet',
        'brinquedoteca'     => 'brinquedoteca',
        'playground'        => 'playground',
        'sala_video_cinema' => 'cinema',
        'sauna_umida'       => 'sauna',
        'sala_fitness'      => 'sala fitness',
    ];

    public function index(Request $r)
    {
        $collection = \Tools::fetchImoveis();

        // Filtro oferta
        if ($r->get('oferta') && in_array($r->get('oferta'), [
            'venda', 'locacao'
        ])) {
            $collection = $collection->filter(function($imovel) use ($r) {
                if ($r->get('oferta') == 'locacao') {
                    return \Tools::imovelLocacao($imovel);
                } else {
                    return \Tools::imovelVenda($imovel);
                }
            });
        }

        // Filtro tipo
        if ($r->get('tipo')) {
            $tipos = \Tools::imoveisUniqueOptions('TipoImovel');
            if (array_key_exists($r->get('tipo'), $tipos)) {
                $collection = $collection->where('TipoImovel', $tipos[$r->get('tipo')]);
            }
        }

        // Filtro cidade
        if ($r->get('cidade')) {
            $cidades = \Tools::imoveisUniqueOptions('Cidade');
            if (array_key_exists($r->get('cidade'), $cidades)) {
                $collection = $collection->where('Cidade', $cidades[$r->get('cidade')]);
            }
        }

        // Filtro bairro
        if ($r->get('bairro') && $r->get('cidade') && array_key_exists($r->get('cidade'), $cidades)) {
            $bairros = \Tools::imoveisUniqueOptions('Bairro', \Tools::fetchImoveis()->where('Cidade', $cidades[$r->get('cidade')]));
            $pesquisa = explode('-', $r->get('bairro'));
            $pesquisa = array_map(function ($bairro) use ($bairros) {
                return array_key_exists($bairro, $bairros) ? $bairros[$bairro] : null;
            }, $pesquisa);
            if (count(array_filter($pesquisa))) {
                $collection = $collection->filter(function($imovel) use ($pesquisa) {
                    return in_array($imovel['Bairro'], $pesquisa);
                });
            }
        }

        // Filtro preço
        if ($r->get('preco-min')) {
            $collection = $collection->filter(function($imovel) use ($r) {
                $preco = isset($imovel['PrecoVenda']) ? $imovel['PrecoVenda'] : 0;
                return $preco >= intval($r->get('preco-min'));
            });
        }
        if ($r->get('preco-max')) {
            $collection = $collection->filter(function($imovel) use ($r) {
                $preco = isset($imovel['PrecoVenda']) ? $imovel['PrecoVenda'] : 0;
                return $preco <= intval($r->get('preco-max'));
            });
        }

        // Filtro metragem
        if ($r->get('area-min')) {
            $collection = $collection->filter(function($imovel) use ($r) {
                return array_key_exists('AreaPrivativa', $imovel) && $imovel['AreaPrivativa'] >= intval($r->get('area-min'));
            });
        }
        if ($r->get('area-max')) {
            $collection = $collection->filter(function($imovel) use ($r) {
                return array_key_exists('AreaPrivativa', $imovel) && $imovel['AreaPrivativa'] <= intval($r->get('area-max'));
            });
        }

        // Filtro dormitórios
        if ($r->get('dormitorios')) {
            if ($r->get('dormitorios') >= '5') {
                $collection = $collection->filter(function($imovel) use ($r) {
                    return $imovel['QtdDormitorios'] >= $r->get('dormitorios');
                });
            } elseif ($r->get('dormitorios') != '0') {
                $collection = $collection->where('QtdDormitorios', $r->get('dormitorios'));
            }
        }

        // Filtro suítes
        if ($r->get('suites')) {
            if ($r->get('suites') >= '5') {
                $collection = $collection->filter(function($imovel) use ($r) {
                    return $imovel['QtdSuites'] >= $r->get('suites');
                });
            } elseif ($r->get('suites') != '0') {
                $collection = $collection->where('QtdSuites', $r->get('suites'));
            }
        }

        // Filtro banheiros
        if ($r->get('banheiros')) {
            if ($r->get('banheiros') >= '5') {
                $collection = $collection->filter(function($imovel) use ($r) {
                    return $imovel['QtdBanheiros'] >= $r->get('banheiros');
                });
            } elseif ($r->get('banheiros') != '0') {
                $collection = $collection->where('QtdBanheiros', $r->get('banheiros'));
            }
        }

        // Filtro vagas
        if ($r->get('vagas')) {
            if ($r->get('vagas') >= '5') {
                $collection = $collection->filter(function($imovel) use ($r) {
                    return $imovel['QtdVagas'] >= $r->get('vagas');
                });
            } elseif ($r->get('vagas') != '0') {
                $collection = $collection->where('QtdVagas', $r->get('vagas'));
            }
        }

        // Filtro código
        if ($r->get('codigo')) {
            $collection = $collection->filter(function($imovel) use($r) {
                return preg_match('/'.$r->get('codigo').'/i', $imovel['CodigoImovel']);
            });
        }

        // Filtro lazer
        if ($r->get('lazer')) {
            $lazer = [
                1  => 'piscina',
                2  => 'piscina_infantil',
                3  => 'salao_festas',
                4  => 'salao_jogos',
                5  => 'churrasqueira',
                6  => 'gourmet',
                7  => 'brinquedoteca',
                8  => 'playground',
                9  => 'sala_video_cinema',
                10 => 'sauna_umida',
                11 => 'sala_fitness',
            ];
            $pesquisa = explode('-', $r->get('lazer'));
            $pesquisa = array_map(function ($index) use ($lazer) {
                return array_key_exists($index, $lazer) ? $lazer[$index] : null;
            }, $pesquisa);
            foreach (array_filter($pesquisa) as $campo) {
                $collection = $collection->where($campo, '1');
            }
        }

        // Filtro condição
        if ($r->get('condicao')) {
            $condicoes = [
                1 => 'Desocupado',
                2 => 'Em construção',
                3 => 'Lançamento'
            ];
            $pesquisa = explode('-', $r->get('condicao'));
            $pesquisa = array_map(function ($index) use ($condicoes) {
                return array_key_exists($index, $condicoes) ? $condicoes[$index] : null;
            }, $pesquisa);
            if (count(array_filter($pesquisa))) {
                $collection = $collection->filter(function($imovel) use ($pesquisa) {
                    return in_array($imovel['Ocupacao'], $pesquisa);
                });
            }
        }

        // Ordenação
        if ($r->get('ordem')) {
            if ($r->get('ordem') == 'asc') {
                $collection = $collection->sortBy('PrecoVenda');
            } elseif ($r->get('ordem') == 'desc') {
                $collection = $collection->sortByDesc('PrecoVenda');
            }
        }

        /* Filtro caracteristicas
        if ($r->get('caracteristicas')) {
            $caracteristicas = [
                1  => 'Terraco',
                2  => 'Sacada',
                3  => 'Lavabo',
                4  => 'PeDireitoDuplo',
                5  => 'Deposito',
                6  => 'Armarios',
                7  => 'ArCondicionado',
                8  => 'Quintal',
                9  => 'Hidromassagem',
                10 => 'Mobiliado',
            ];
            $pesquisa = explode('-', $r->get('caracteristicas'));
            $pesquisa = array_map(function ($index) use ($caracteristicas) {
                return array_key_exists($index, $caracteristicas) ? $caracteristicas[$index] : null;
            }, $pesquisa);
            foreach (array_filter($pesquisa) as $campo) {
                if ($campo == 'Armarios') {
                    $tiposArmario = ['ArmarioCorredor', 'ArmarioCloset', 'ArmarioDormitorio', 'ArmarioBanheiro', 'ArmarioSala', 'ArmarioEscritorio', 'ArmarioHomeTheater', 'ArmarioDormitorioEmpregada', 'ArmarioAreaServico'];

                    $collection = $collection->filter(function($imovel) use ($tiposArmario) {
                        foreach($tiposArmario as $tipo) {
                            if (array_key_exists($tipo, $imovel) && $imovel[$tipo] == '1') {
                                return true;
                            }
                        }

                        return false;
                    });
                } else {
                    $collection = $collection->where($campo, '1');
                }
            }
        }*/

        $imoveis = $this->paginate($collection, 12, null, [
            'path'  => 'imoveis',
            'query' => [
                'avancada'        => $r->get('avancada'),
                'tipo'            => $r->get('tipo'),
                'cidade'          => $r->get('cidade'),
                'bairro'          => $r->get('bairro'),
                'preco-min'       => $r->get('preco-min'),
                'preco-max'       => $r->get('preco-max'),
                'area-min'        => $r->get('area-min'),
                'area-max'        => $r->get('area-max'),
                'dormitorios'     => $r->get('dormitorios'),
                'suites'          => $r->get('suites'),
                'banheiros'       => $r->get('banheiros'),
                'vagas'           => $r->get('vagas'),
                'codigo'          => $r->get('codigo'),
                'lazer'           => $r->get('lazer'),
                'condicao'        => $r->get('condicao'),
                'ordem'           => $r->get('ordem'),
                'oferta'          => $r->get('oferta'),
                // 'caracteristicas' => $r->get('caracteristicas'),
            ]
        ]);

        $caracteristicas = $this->caracteristicas;

        return view('frontend.imoveis.index', compact('imoveis', 'caracteristicas'));
    }

    public function show($codigo)
    {
        $imoveis = \Tools::fetchImoveis();
        $imovel  = $imoveis->where('CodigoImovel', $codigo)->first();

        if (!$imovel) abort('404');

        $caracteristicas = $this->caracteristicas;
        $lazer = [];
        foreach ($caracteristicas as $caracteristica => $texto) {
            if (array_key_exists($caracteristica, $imovel) && $imovel[$caracteristica] == '1') {
                $lazer[$caracteristica] = $texto;
            }
        }

        if (array_key_exists('Fotos', $imovel)) {
            $fotos = array_key_exists(0, $imovel['Fotos']['Foto'])
                ? $imovel['Fotos']['Foto']
                : [$imovel['Fotos']['Foto']];
        } else {
            $fotos = [];
        }
        $fotos = collect($fotos);

        $galeria = $fotos->filter(function($value, $key) {
            if (!array_key_exists('FotoTitulo', $value)) return true;
            return !preg_match('/planta/i', $value['FotoTitulo']);
        });
        $plantas = $fotos->filter(function($value, $key) {
            if (!array_key_exists('FotoTitulo', $value)) return false;
            return preg_match('/Planta/i', $value['FotoTitulo']);
        });

        $ofertas = $imoveis
            ->filter(function($q) use ($imovel) {
                return Tools::imovelLocacao($imovel)
                    ? Tools::imovelLocacao($q)
                    : Tools::imovelVenda($q);
            })
            ->shuffle()
            ->filter(function($value, $key) use ($codigo) {
                return $value['CodigoImovel'] != $codigo;
            })
            ->take(6);

        return view('frontend.imoveis.show', compact('imovel', 'lazer', 'galeria', 'plantas', 'ofertas'));
    }
}
