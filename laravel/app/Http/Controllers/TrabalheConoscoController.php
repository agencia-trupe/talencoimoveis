<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CurriculosRecebidosRequest;

use App\Models\Contato;
use App\Models\CurriculoRecebido;
use App\Models\TrabalheConosco;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $trabalheConosco = TrabalheConosco::first();

        return view('frontend.trabalhe-conosco', compact('trabalheConosco'));
    }

    public function post(CurriculosRecebidosRequest $request, CurriculoRecebido $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('curriculo')) {
            $input['curriculo'] = CurriculoRecebido::upload_curriculo();
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.curriculo', $input, function($message) use ($request, $contato)
            {
                $message->to('rh@talenco.com.br', config('site.name'))
                        ->subject('[CURRÍCULO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }
}
