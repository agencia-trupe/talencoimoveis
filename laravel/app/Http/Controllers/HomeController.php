<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NewsletterRequest;
use App\Http\Controllers\Controller;

use App\Models\Newsletter;
use App\Models\Contato;
use App\Models\Banner;
use App\Models\Destaques;
use App\Models\Post;

class HomeController extends Controller
{
    public function index()
    {
        $contato   = Contato::first();
        $banners   = Banner::ordenados()->get();
        $blog      = Post::ordenados()->publicados()->take(3)->get();

        $imoveis   = \Tools::fetchImoveis()
            ->where('TipoOferta', '2')->shuffle();

        $destaques = [
            'locacao'            => $imoveis->filter(function($imovel) {
                return \Tools::imovelLocacao($imovel);
            })->take(3),
            'prontos_para_morar' => $imoveis->where('Ocupacao', 'Desocupado')->filter(function($imovel) {
                return \Tools::imovelVenda($imovel);
            })->take(3),
            'em_construcao'      => $imoveis->where('Ocupacao', 'Em construção')->filter(function($imovel) {
                return \Tools::imovelVenda($imovel);
            })->take(3),
            'lancamentos'        => $imoveis->where('Ocupacao', 'Lançamento')->filter(function($imovel) {
                return \Tools::imovelVenda($imovel);
            })->take(3),
        ];

        return view('frontend.home', compact('contato', 'banners', 'blog', 'destaques'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'Cadastro efetuado com sucesso!'
        ];

        return response()->json($response);
    }
}
