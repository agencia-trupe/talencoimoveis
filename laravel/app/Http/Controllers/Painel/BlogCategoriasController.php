<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BlogCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\PostCategoria;

class BlogCategoriasController extends Controller
{
    public function index()
    {
        $categorias = PostCategoria::ordenados()->get();

        return view('painel.blog.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.blog.categorias.create');
    }

    public function store(BlogCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = PostCategoria::upload_capa();

            PostCategoria::create($input);
            return redirect()->route('painel.blog.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(PostCategoria $categoria)
    {
        return view('painel.blog.categorias.edit', compact('categoria'));
    }

    public function update(BlogCategoriasRequest $request, PostCategoria $categoria)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = PostCategoria::upload_capa();

            $categoria->update($input);
            return redirect()->route('painel.blog.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(PostCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.blog.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
