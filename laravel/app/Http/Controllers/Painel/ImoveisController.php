<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ImovelRecebido;

class ImoveisController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ImovelRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.imoveis.index', compact('contatosrecebidos'));
    }

    public function show(ImovelRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.imoveis.show', compact('contato'));
    }

    public function destroy(ImovelRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.imoveis.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ImovelRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.imoveis.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
