<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpresaRequest;
use App\Http\Controllers\Controller;

use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        $registro = Empresa::first();

        return view('painel.empresa.edit', compact('registro'));
    }

    public function update(EmpresaRequest $request, Empresa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_construcao'])) $input['imagem_construcao'] = Empresa::upload_imagem_construcao();
            if (isset($input['imagem_reformas'])) $input['imagem_reformas'] = Empresa::upload_imagem_reformas();
            if (isset($input['imagem_negocios'])) $input['imagem_negocios'] = Empresa::upload_imagem_negocios();

            $registro->update($input);

            return redirect()->route('painel.empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
