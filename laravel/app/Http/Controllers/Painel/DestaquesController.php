<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DestaquesRequest;
use App\Http\Controllers\Controller;

use App\Models\Destaques;

class DestaquesController extends Controller
{
    public function index()
    {
        $registro = Destaques::first();

        return view('painel.destaques.edit', compact('registro'));
    }

    public function update(DestaquesRequest $request, Destaques $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.destaques.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
