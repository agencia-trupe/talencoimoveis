<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CurriculoRecebido;

class CurriculosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = CurriculoRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.curriculos.index', compact('contatosrecebidos'));
    }

    public function show(CurriculoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.curriculos.show', compact('contato'));
    }

    public function destroy(CurriculoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.curriculos.index')->with('success', 'Currículo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir currículo: '.$e->getMessage()]);

        }
    }

    public function toggle(CurriculoRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.curriculos.index')->with('success', 'Currículo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar currículo: '.$e->getMessage()]);

        }
    }
}
