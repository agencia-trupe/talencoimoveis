<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ImoveisRecebidosRequest;

use App\Models\Contato;
use App\Models\ImovelRecebido;

class AnuncieController extends Controller
{
    public function index()
    {
        return view('frontend.anuncie');
    }

    public function post(ImoveisRecebidosRequest $request, ImovelRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.imovel', $input, function($message) use ($request, $contato)
            {
                $message->to('atendimento@talencoimoveis.com.br', config('site.name'))
                        ->subject('[IMÓVEL] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }
}
