<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Post;
use App\Models\PostCategoria;

class BlogController extends Controller
{
    public function __construct()
    {
        view()->share('categorias', PostCategoria::ordenados()->get());
    }

    public function index(PostCategoria $categoria)
    {
        if (request()->get('busca')) {
            $posts = Post::busca(request()->get('busca'))->ordenados()->publicados()->paginate(14);
        } elseif ($categoria->exists) {
            $posts = $categoria->blog()->publicados()->paginate(14);
        } else {
            $posts = Post::ordenados()->publicados()->paginate(14);
        }

        if (request()->get('page') && !request()->ajax()) abort('404');

        if (request()->ajax()) {
            return [
                'posts'    => view()->make('frontend.blog._posts', compact('posts'))->render(),
                'nextPage' => $posts->hasMorePages() ? route('blog', [
                    null,
                    'page'  => $posts->currentPage() + 1,
                    'busca' => request()->get('busca')
                ]) : false
            ];
        }

        return view('frontend.blog.index', compact('categoria', 'posts'));
    }

    public function show(PostCategoria $categoria, Post $post)
    {
        $leiaMais = Post::ordenados()->where('id', '!=', $post->id)->take(3)->get();

        view()->share('blogPost', $post);

        return view('frontend.blog.show', compact('categoria', 'post', 'leiaMais'));
    }
}
