<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'link' => 'required',
            'subtitulo' => '',
            'data_de_entrega' => '',
            'nome_do_empreendimento' => 'required',
            'titulo' => 'required',
            'area' => '',
            'valor' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
