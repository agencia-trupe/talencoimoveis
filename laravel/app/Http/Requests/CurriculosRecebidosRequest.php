<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CurriculosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'email'     => 'required|email',
            'curriculo' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'      => 'insira seu nome',
            'email.required'     => 'insira seu e-mail',
            'email.email'        => 'insira um endereço de e-mail válido',
            'curriculo.required' => 'o arquivo de currículo é obrigatório',
        ];
    }
}
