<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => 'insira seu nome',
            'email.required'    => 'insira seu e-mail',
            'email.email'       => 'insira um endereço de e-mail válido',
            'mensagem.required' => 'insira uma mensagem'
        ];
    }
}
