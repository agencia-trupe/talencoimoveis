<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DestaquesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'prontos_para_morar_1' => '',
            'prontos_para_morar_2' => '',
            'prontos_para_morar_3' => '',
            'em_construcao_1' => '',
            'em_construcao_2' => '',
            'em_construcao_3' => '',
            'lancamentos_1' => '',
            'lancamentos_2' => '',
            'lancamentos_3' => '',
        ];
    }
}
