<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImoveisRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'                 => 'required',
            'email'                => 'required|email',
            'cep'                  => 'required',
            'cidade'               => 'required',
            'bairro'               => 'required',
            'logradouro'           => 'required',
            'numero'               => 'required',
            'tipo'                 => 'required',
            'negocio'              => 'required',
            'escritura_definitiva' => 'required',
            'financiado'           => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome.required'                 => 'insira seu nome',
            'email.required'                => 'insira seu e-mail',
            'email.email'                   => 'insira um endereço de e-mail válido',
            'cep.required'                  => 'preencha o CEP',
            'cidade.required'               => 'preencha a cidade',
            'bairro.required'               => 'preencha o bairro',
            'logradouro.required'           => 'preencha o logradouro',
            'numero.required'               => 'preencha o número',
            'tipo.required'                 => 'selecione o tipo de imóvel',
            'negocio.required'              => 'selecione o tipo de negócio',
            'escritura_definitiva.required' => 'informe se o imóvel possui escritura definitiva',
            'financiado.required'           => 'informe se o imóvel é financiado',
        ];
    }
}
