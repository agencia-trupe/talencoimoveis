<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'codigo_video'            => 'required',
            'texto_1'                 => 'required',
            'texto_2'                 => 'required',
            'unidades_de_negocio'     => 'required',
            'imagem_construcao'       => 'image',
            'texto_construcao'        => 'required',
            'imagem_reformas'         => 'image',
            'texto_reformas'          => 'required',
            'imagem_negocios'         => 'image',
            'texto_negocios'          => 'required',
            'responsabilidade_social' => 'required',
        ];
    }
}
