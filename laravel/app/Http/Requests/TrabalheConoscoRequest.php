<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TrabalheConoscoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'texto' => 'required',
        ];
    }
}
