<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });
        view()->composer('frontend.common.busca', function($view) {
            $view->with('tipos', \Tools::imoveisUniqueOptions('TipoImovel'));
            $view->with('cidades', \Tools::imoveisUniqueOptions('Cidade'));
            $view->with('bairrosPorCidade', \Tools::imoveisBairrosPorCidade());
        });
        view()->composer('frontend.common.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('curriculosNaoLidos', \App\Models\CurriculoRecebido::naoLidos()->count());
            $view->with('imoveisNaoLidos', \App\Models\ImovelRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
