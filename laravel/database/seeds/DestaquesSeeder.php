<?php

use Illuminate\Database\Seeder;

class DestaquesSeeder extends Seeder
{
    public function run()
    {
        DB::table('destaques')->insert([
            'prontos_para_morar_1' => '',
            'prontos_para_morar_2' => '',
            'prontos_para_morar_3' => '',
            'em_construcao_1' => '',
            'em_construcao_2' => '',
            'em_construcao_3' => '',
            'lancamentos_1' => '',
            'lancamentos_2' => '',
            'lancamentos_3' => '',
        ]);
    }
}
