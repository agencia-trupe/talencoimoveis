<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefone'    => '13 3395 0005',
            'whatsapp'    => '13 93395 0005',
            'email'       => 'contato@trupe.net',
            'endereco'    => 'Rua do Endereço Completo, 123 &middot; Vila do Bairro &middot; São Paulo, SP &middot; 01234-567',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.145831367559!2d-46.65644434884712!3d-23.563205367471678!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c8da0aa315%3A0xd59f9431f2c9776a!2sAv.+Paulista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1507303225288" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook'    => '#facebook',
            'linkedin'    => '#linkedin',
            'instagram'   => '#instagram',
            'pinterest'   => '#pinterest',
            'youtube'     => '#youtube',
        ]);
    }
}
