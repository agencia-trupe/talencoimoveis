<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveisRecebidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis_recebidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone_residencial');
            $table->string('telefone_celular');
            $table->string('whatsapp');
            $table->string('cidade_proprietario');
            $table->string('cep');
            $table->string('cidade');
            $table->string('bairro');
            $table->string('logradouro');
            $table->string('numero');
            $table->string('complemento');
            $table->string('tipo');
            $table->string('negocio');
            $table->string('valor_imovel');
            $table->string('valor_condominio');
            $table->string('valor_iptu');
            $table->string('area_util');
            $table->string('area_total');
            $table->string('dormitorios');
            $table->string('suites');
            $table->string('banheiros');
            $table->string('vagas');
            $table->string('tipo_vagas');
            $table->string('ano_construcao');
            $table->string('elevadores');
            $table->string('lazer');
            $table->string('escritura_definitiva');
            $table->string('financiado');
            $table->text('mensagem');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imoveis_recebidos');
    }
}
