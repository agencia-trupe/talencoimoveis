<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('link');
            $table->text('subtitulo');
            $table->string('legenda_1');
            $table->string('nome_do_empreendimento');
            $table->text('titulo');
            $table->string('area');
            $table->string('valor');
            $table->string('legenda_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
