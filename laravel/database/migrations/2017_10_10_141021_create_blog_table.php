<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    public function up()
    {
        Schema::create('blog_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('blog_categoria_id')->unsigned()->nullable();
            $table->string('slug');
            $table->string('data');
            $table->string('titulo');
            $table->text('description');
            $table->text('keywords');
            $table->string('capa');
            $table->text('texto');
            $table->foreign('blog_categoria_id')->references('id')->on('blog_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('blog');
        Schema::drop('blog_categorias');
    }
}
