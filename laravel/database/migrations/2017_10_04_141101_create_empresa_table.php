<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_video');
            $table->text('texto_1');
            $table->text('texto_2');
            $table->text('unidades_de_negocio');
            $table->string('imagem_construcao');
            $table->text('texto_construcao');
            $table->string('imagem_reformas');
            $table->text('texto_reformas');
            $table->string('imagem_negocios');
            $table->text('texto_negocios');
            $table->text('responsabilidade_social');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
