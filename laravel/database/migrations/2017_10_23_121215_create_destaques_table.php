<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prontos_para_morar_1');
            $table->string('prontos_para_morar_2');
            $table->string('prontos_para_morar_3');
            $table->string('em_construcao_1');
            $table->string('em_construcao_2');
            $table->string('em_construcao_3');
            $table->string('lancamentos_1');
            $table->string('lancamentos_2');
            $table->string('lancamentos_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('destaques');
    }
}
