export default function Busca() {
    // Seleciona bairros por cidade
    $('select[name=cidade]').change(function() {
        $('.busca-bairros-lista label').removeClass('active')
            .find('input').prop('checked', false);
        $('input[name=bairros-todos]').prop('checked', false);

        if (this.value) {
            $('.busca-bairros-lista label[data-cidade=' + this.value + ']')
                .addClass('active');
            if ($('.busca-avancada').is(':visible')) {
                $('.busca-bairros').slideDown();
            }
        } else {
            $('.busca-bairros').slideUp();
        }
    });

    if ($('select[name=cidade]').value) {
        $('.busca-bairros-lista label[data-cidade=' + $('select[name=cidade]').value + ']')
            .addClass('active');
    }

    // Botão selecionar todos bairros
    $('input[name=bairros-todos]').click(function() {
        $('.bairro-label.active input').prop('checked', this.checked);
    });
    $('input[name="bairro[]"]').click(function() {
        if ($('.bairro-label.active input:checked').length == $('.bairro-label.active input').length) {
            $('input[name=bairros-todos]').prop('checked', true);
        } else {
            $('input[name=bairros-todos]').prop('checked', false);
        }
    });

    // Mostrar/esconder bairros
    $('.handle-mostra-bairros').click(function(event) {
        event.preventDefault();

        if (!$('select[name=cidade]').val()) {
            return alert('Selecione uma cidade para visualizar os bairros disponíveis.');
        }

        $('.busca-bairros').slideDown();
    });
    $('.busca-bairros-fechar').click(function(event) {
        event.preventDefault();
        $('.busca-bairros').slideUp(function() {
            $('input[name="bairro[]"]').prop('checked', false);
        });
    });

    // Mostrar/esconder busca avançada
    $('.handle-busca-avancada').click(function(event) {
        event.preventDefault();

        if ($('.busca-avancada').is(':visible')) {
            return $('.busca-bairros, .busca-avancada').slideUp();
        }

        if ($('select[name=cidade]').val()) {
            $('.busca-bairros').slideDown();
        }

        $('.busca-avancada').slideDown();
    });

    // Desmarca checkbox
    $('input[name=dormitorios]').on('change', function() {
        $('input[name=dormitorios]').not(this).prop('checked', false);
    });
    $('input[name=suites]').on('change', function() {
        $('input[name=suites]').not(this).prop('checked', false);
    });
    $('input[name=banheiros]').on('change', function() {
        $('input[name=banheiros]').not(this).prop('checked', false);
    });
    $('input[name=vagas]').on('change', function() {
        $('input[name=vagas]').not(this).prop('checked', false);
    });

    // Limpar seleção
    $('.limpar').click(function(event) {
        event.preventDefault();

        $('.busca-bairros').slideUp(function() {
            $('select').val('');
            $('input').val('');
            $('input:checked').prop('checked', false);
        });
    });

    // Envia busca simples
    $('.envia-busca-simples').click(function(event) {
        event.preventDefault();

        if (!$('select[name=tipo]').val() && !$('select[name=cidade]').val()) {
            return alert('Selecione um tipo de imóvel ou localização para realizar a busca.');
        }

        var url = $('.busca').data('url'),
            params = {};

        if ($('select[name=oferta]').val()) {
            params.oferta = $('select[name=oferta]').val();
        }
        if ($('select[name=tipo]').val()) {
            params.tipo = $('select[name=tipo]').val();
        }
        if ($('select[name=cidade]').val()) {
            params.cidade = $('select[name=cidade]').val();
        }

        if ($('input[name="bairro[]"]:checked').length && !$('input[name=bairros-todos]').prop('checked')) {
            var bairros = [];
            $('input[name="bairro[]"]:checked').each(function(index, el) {
                bairros.push(el.value);
            });

            params.bairro = bairros.join('-');
        }

        window.location = url + '?' + $.param(params);
    });

    // Envia busca simples
    $('.envia-busca-avancada').click(function(event) {
        event.preventDefault();

        var url    = $('.busca').data('url'),
            params = {};

        if (!$('select[name=tipo]').val() && !$('select[name=cidade]').val() && !$('input[name=codigo]').val()) {
            return alert('Selecione um tipo de imóvel ou localização para realizar a busca.');
        }

        if ($('input[name=codigo]').val()) {
            return window.location = url + '?codigo=' + $('input[name=codigo]').val();
        }

        if ($('select[name=oferta]').val()) {
            params.oferta = $('select[name=oferta]').val();
        }
        if ($('select[name=tipo]').val()) {
            params.tipo = $('select[name=tipo]').val();
        }
        if ($('select[name=cidade]').val()) {
            params.cidade = $('select[name=cidade]').val();
        }

        if ($('input[name="bairro[]"]:checked').length && !$('input[name=bairros-todos]').prop('checked')) {
            var bairros = [];
            $('input[name="bairro[]"]:checked').each(function(index, el) {
                bairros.push(el.value);
            });

            params.bairro = bairros.join('-');
        }

        if ($('input[name=preco-min]').val()) {
            params['preco-min'] = $('input[name=preco-min]').val();
        }
        if ($('input[name=preco-max]').val()) {
            params['preco-max'] = $('input[name=preco-max]').val();
        }

        if ($('input[name=area-min]').val()) {
            params['area-min'] = $('input[name=area-min]').val();
        }
        if ($('input[name=area-max]').val()) {
            params['area-max'] = $('input[name=area-max]').val();
        }

        if ($('input[name=dormitorios]:checked').length) {
            params.dormitorios = $('input[name=dormitorios]:checked').val();
        }
        if ($('input[name=suites]:checked').length) {
            params.suites = $('input[name=suites]:checked').val();
        }
        if ($('input[name=banheiros]:checked').length) {
            params.banheiros = $('input[name=banheiros]:checked').val();
        }
        if ($('input[name=vagas]:checked').length) {
            params.vagas = $('input[name=vagas]:checked').val();
        }

        if ($('input[name="lazer[]"]:checked').length) {
            var lazer = [];
            $('input[name="lazer[]"]:checked').each(function(index, el) {
                lazer.push(el.value);
            });

            params.lazer = lazer.join('-');
        }

        if ($('input[name="condicao[]"]:checked').length) {
            var condicao = [];
            $('input[name="condicao[]"]:checked').each(function(index, el) {
                condicao.push(el.value);
            });

            params.condicao = condicao.join('-');
        }

        if ($('input[name="caracteristicas[]"]:checked').length) {
            var caracteristicas = [];
            $('input[name="caracteristicas[]"]:checked').each(function(index, el) {
                caracteristicas.push(el.value);
            });

            params.caracteristicas = caracteristicas.join('-');
        }

        if ($('input[name=ordem]:checked').length) {
            params.ordem = $('input[name=ordem]:checked').val();
        }

        window.location = url + '?' + $.param(params);
    });
};
