import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Newsletter from './Newsletter';
import Busca from './Busca';

AjaxSetup();
MobileToggle();
Newsletter();
Busca();

$('.banners-cycle').cycle({
    slides: '>.slide'
});

$('.blog-ver-mais').click(function(event) {
    event.preventDefault();

    var $btn       = $(this),
        $container = $('.blog-posts');

    if ($btn.hasClass('loading')) return false;

    $btn.addClass('loading');

    $.get($btn.data('next'), function(data) {
        var posts = $(data.posts).hide();
        $container.append(posts);
        posts.fadeIn();

        $btn.removeClass('loading');

        if (data.nextPage) {
            $btn.data('next', data.nextPage);
        } else {
            $btn.fadeOut(function() {
                $this.remove();
            });
        }
    });
});

$('#curriculo input').on('change', function(e) {
    var file = e.target.files[0];

    if (file) {
        $('#curriculo span').html(file.name).parent().addClass('active');
    } else {
        $('#curriculo span').html('ANEXAR CURRÍCULO').parent().removeClass('active');
    }
});

Inputmask.extendDefaults({ jitMasking: true });
$('input[name=cep]').inputmask('99999-999');

var validaCep = function(input) {
    input = input.replace(/\D/g, '');
    return /^\d{8}$/.test(input);
};

$('.cep a').click(function(event) {
    event.preventDefault();

    var cep = $('input[name=cep]').val();

    if (!validaCep(cep)) {
        alert('Insira um CEP válido.');
        return;
    }

    $.getJSON('//viacep.com.br/ws/'+ cep +'/json/?callback=?', function(dados) {
        if (!('erro' in dados)) {
            $('input[name="cidade"]').val(dados.localidade);
            $('input[name="bairro"]').val(dados.bairro);
            $('input[name="logradouro"]').val(dados.logradouro);
        } else {
            $('input[name=cep]').val('');
            alert('CEP não encontrado. Tente novamente.');
        }
    });
});

$(".fancybox").fancybox();

$('.imagens-cycle').cycle({
    slides: '>div',
    next: '#next',
    prev: '#prev',
    pager: '#pager',
    timeout: 0
});

$('.imagens-carousel').cycle({
    fx: 'carousel',
    slides: '>div',
    carouselVisible: 4,
    carouselFluid: true,
    allowWrap: false,
    timeout: 0
});

$('.imagens-cycle').on('cycle-next cycle-prev', function(e, opts) {
    $('.imagens-carousel').cycle('goto', opts.currSlide);
});

$('.imagens-carousel .cycle-slide').click(function(){
    var index = $('.imagens-carousel').data('cycle.API').getSlideIndex(this);
    $('.imagens-cycle, .imagens-carousel').cycle('goto', index);
});

if ($('.banners').length) {
    $(window).resize(function() {
        var altura   = $(window).height(),
            $banners = $('.banners');

        if (!$('.mobile-check').is(':visible') && (altura <= 685)) {
            var resize = ((altura - 185) < 265 ? 265 : (altura - 185));
            $banners.css('height', resize + 'px');
        } else {
            $banners.css('height', '');
        }
    }).trigger('resize');
}
