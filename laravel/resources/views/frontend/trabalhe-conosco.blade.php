@extends('frontend.common.template')

@section('content')

    <div class="trabalhe-conosco">
        <div class="center">
            <div class="left">
                <h2>{{ $trabalheConosco->titulo }}</h2>
                {!! $trabalheConosco->texto !!}
            </div>
            <div class="right">
                <h3>JUNTE-SE A NÓS</h3>
                <p>Envie seu currículo agora mesmo!</p>

                @if(session('success'))
                <div class="curriculo-enviado">
                    <p>Currículo enviado com sucesso!</p>
                </div>
                @else
                <form action="{{ route('trabalhe.post') }}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                        </div>
                    @endif

                    {!! Form::text('nome', null, ['placeholder' => 'nome', 'required' => true]) !!}
                    {!! Form::email('email', null, ['placeholder' => 'e-mail',  'required' => true]) !!}
                    {!! Form::text('telefone', null, ['placeholder' => 'telefone']) !!}
                    <label id="curriculo">
                        <span>ANEXAR CURRÍCULO</span>
                        <input type="file" name="curriculo">
                    </label>
                    <input type="submit" value="ENVIAR">
                </form>
                @endif
            </div>
        </div>
    </div>

@endsection
