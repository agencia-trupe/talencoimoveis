<div class="primaria">
    <a href="{{ route('imoveis') }}" @if(Tools::isActive('imoveis*')) class="active" @endif>ENCONTRE <span>SEU IMÓVEL</span></a>
    <a href="{{ route('anuncie') }}" @if(Tools::isActive('anuncie')) class="active" @endif>ANUNCIE <span>SEU IMÓVEL</span></a>
    <a href="{{ route('blog') }}" @if(Tools::isActive('blog*')) class="active" @endif>ESPAÇO <span>TALENCO</span></a>
</div>
<div class="secundaria">
    <a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>A TALENCO</a>
    <a href="{{ route('trabalhe') }}" @if(Tools::isActive('trabalhe')) class="active" @endif>TRABALHE CONOSCO</a>
    <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
</div>
<div class="header-social">
    @foreach(['facebook', 'linkedin', 'instagram', 'pinterest', 'youtube'] as $s)
    @if($contato->{$s})
    <a href="{{ $contato->{$s} }}" class="social {{ $s }}">{{ $s }}</a>
    @endif
    @endforeach
</div>
