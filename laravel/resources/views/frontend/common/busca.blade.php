<div class="busca" data-url="{{ route('imoveis') }}">
    <div class="center">
        <div class="busca-principal">
            <div class="busca-selects">
                <span>QUERO</span>
                {!! Form::select('oferta', ['venda' => 'COMPRAR', 'locacao' => 'ALUGAR'], Request::get('oferta')) !!}
                <span>UM IMÓVEL</span>
                {!! Form::select('tipo', $tipos, Request::get('tipo'), ['placeholder' => 'TIPO...']) !!}
                {!! Form::select('cidade', $cidades, Request::get('cidade'), ['placeholder' => 'CIDADE...']) !!}
                <a href="#" class="handle-mostra-bairros">BAIRRO...</a>
            </div>
            <div class="botao-busca-simples">
                <a href="#" class="envia-busca-simples">BUSCAR</a>
            </div>
            <a href="#" class="handle-busca-avancada">BUSCA AVANÇADA</a>
        </div>
        <div class="busca-bairros">
            <h3>BAIRROS</h3>
            <label>
                <input type="checkbox" name="bairros-todos">
                Selecionar todos
            </label>
            <a href="#" class="busca-bairros-fechar">fechar</a>

            <div class="busca-bairros-lista">
                <?php $i = 0; $bairrosSelecionados = explode('-', Request::get('bairro')); ?>
                @foreach($bairrosPorCidade as $cidade => $bairros)
                    <?php $i++; ?>
                    @foreach($bairros as $keyBairro => $bairro)
                        <label class="bairro-label @if(Request::get('cidade') == $i) active @endif" data-cidade="{{ $i }}">
                            <input type="checkbox" name="bairro[]" value="{{ $keyBairro }}" @if(Request::get('cidade') == $i && in_array($keyBairro, $bairrosSelecionados)) checked @endif>
                            {{ $bairro }}
                        </label>
                    @endforeach
                @endforeach
            </div>
        </div>
        <div class="busca-avancada">
            <div class="bloco-1">
                <div class="col">
                    <div class="preco">
                        <span>FAIXA DE PREÇO</span>
                        <input type="text" name="preco-min" placeholder="De (R$)" value="{{ Request::get('preco-min') }}" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                        <input type="text" name="preco-max" placeholder="Até (R$)" value="{{ Request::get('preco-max') }}" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                    </div>
                    <div class="metragem">
                        <span>METRAGEM</span>
                        <input type="text" name="area-min" placeholder="De (m²)" value="{{ Request::get('area-min') }}" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                        <input type="text" name="area-max" placeholder="Até (m²)" value="{{ Request::get('area-max') }}" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                    </div>
                </div>
                <div class="col">
                    <div class="dormitorios">
                        <span>DORMITÓRIOS</span>
                        <label>
                            <input type="checkbox" name="dormitorios" value="0" @if(Request::get('dormitorios') == '0') checked @endif>
                            <span>0</span>
                        </label>
                        <label>
                            <input type="checkbox" name="dormitorios" value="1" @if(Request::get('dormitorios') == '1') checked @endif>
                            <span>1</span>
                        </label>
                        <label>
                            <input type="checkbox" name="dormitorios" value="2" @if(Request::get('dormitorios') == '2') checked @endif>
                            <span>2</span>
                        </label>
                        <label>
                            <input type="checkbox" name="dormitorios" value="3" @if(Request::get('dormitorios') == '3') checked @endif>
                            <span>3</span>
                        </label>
                        <label>
                            <input type="checkbox" name="dormitorios" value="4" @if(Request::get('dormitorios') == '4') checked @endif>
                            <span>4</span>
                        </label>
                        <label>
                            <input type="checkbox" name="dormitorios" value="5" @if(Request::get('dormitorios') == '5') checked @endif>
                            <span>5+</span>
                        </label>
                    </div>
                    <div class="banheiros">
                        <span>BANHEIROS</span>
                        <label>
                            <input type="checkbox" name="banheiros" value="0" @if(Request::get('banheiros') == '0') checked @endif>
                            <span>0</span>
                        </label>
                        <label>
                            <input type="checkbox" name="banheiros" value="1" @if(Request::get('banheiros') == '1') checked @endif>
                            <span>1</span>
                        </label>
                        <label>
                            <input type="checkbox" name="banheiros" value="2" @if(Request::get('banheiros') == '2') checked @endif>
                            <span>2</span>
                        </label>
                        <label>
                            <input type="checkbox" name="banheiros" value="3" @if(Request::get('banheiros') == '3') checked @endif>
                            <span>3</span>
                        </label>
                        <label>
                            <input type="checkbox" name="banheiros" value="4" @if(Request::get('banheiros') == '4') checked @endif>
                            <span>4</span>
                        </label>
                        <label>
                            <input type="checkbox" name="banheiros" value="5" @if(Request::get('banheiros') == '5') checked @endif>
                            <span>5+</span>
                        </label>
                    </div>
                    <div class="suites">
                        <span>SUÍTES</span>
                        <label>
                            <input type="checkbox" name="suites" value="0" @if(Request::get('suites') == '0') checked @endif>
                            <span>0</span>
                        </label>
                        <label>
                            <input type="checkbox" name="suites" value="1" @if(Request::get('suites') == '1') checked @endif>
                            <span>1</span>
                        </label>
                        <label>
                            <input type="checkbox" name="suites" value="2" @if(Request::get('suites') == '2') checked @endif>
                            <span>2</span>
                        </label>
                        <label>
                            <input type="checkbox" name="suites" value="3" @if(Request::get('suites') == '3') checked @endif>
                            <span>3</span>
                        </label>
                        <label>
                            <input type="checkbox" name="suites" value="4" @if(Request::get('suites') == '4') checked @endif>
                            <span>4</span>
                        </label>
                        <label>
                            <input type="checkbox" name="suites" value="5" @if(Request::get('suites') == '5') checked @endif>
                            <span>5+</span>
                        </label>
                    </div>
                    <div class="vagas">
                        <span>VAGAS</span>
                        <label>
                            <input type="checkbox" name="vagas" value="0" @if(Request::get('vagas') == '0') checked @endif>
                            <span>0</span>
                        </label>
                        <label>
                            <input type="checkbox" name="vagas" value="1" @if(Request::get('vagas') == '1') checked @endif>
                            <span>1</span>
                        </label>
                        <label>
                            <input type="checkbox" name="vagas" value="2" @if(Request::get('vagas') == '2') checked @endif>
                            <span>2</span>
                        </label>
                        <label>
                            <input type="checkbox" name="vagas" value="3" @if(Request::get('vagas') == '3') checked @endif>
                            <span>3</span>
                        </label>
                        <label>
                            <input type="checkbox" name="vagas" value="4" @if(Request::get('vagas') == '4') checked @endif>
                            <span>4</span>
                        </label>
                        <label>
                            <input type="checkbox" name="vagas" value="5" @if(Request::get('vagas') == '5') checked @endif>
                            <span>5+</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="bloco-2">
                <div class="col">
                    <div class="lazer">
                        <?php $lazerSelecionados = explode('-', Request::get('lazer')); ?>
                        <span>LAZER</span>
                        <label>
                            <input type="checkbox" name="lazer[]" value="1" @if(in_array('1', $lazerSelecionados)) checked @endif>
                            <span>Piscina</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="2" @if(in_array('2', $lazerSelecionados)) checked @endif>
                            <span>Piscina Infantil</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="3" @if(in_array('3', $lazerSelecionados)) checked @endif>
                            <span>Salão de festas</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="4" @if(in_array('4', $lazerSelecionados)) checked @endif>
                            <span>Salão de jogos</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="5" @if(in_array('5', $lazerSelecionados)) checked @endif>
                            <span>Churrasqueira</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="6" @if(in_array('6', $lazerSelecionados)) checked @endif>
                            <span>Espaço gourmet</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="7" @if(in_array('7', $lazerSelecionados)) checked @endif>
                            <span>Brinquedoteca</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="8" @if(in_array('8', $lazerSelecionados)) checked @endif>
                            <span>Playground</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="9" @if(in_array('9', $lazerSelecionados)) checked @endif>
                            <span>Cinema</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="10" @if(in_array('10', $lazerSelecionados)) checked @endif>
                            <span>Sauna</span>
                        </label>
                        <label>
                            <input type="checkbox" name="lazer[]" value="11" @if(in_array('11', $lazerSelecionados)) checked @endif>
                            <span>Sala fitness</span>
                        </label>
                    </div>
                </div>
                <div class="col">
                    <div class="condicao">
                        <?php $condicaoSelecionados = explode('-', Request::get('condicao')); ?>
                        <span>CONDIÇÃO DO IMÓVEL</span>
                        <label>
                            <input type="checkbox" name="condicao[]" value="1" @if(in_array('1', $condicaoSelecionados)) checked @endif>
                            <span>Pronto para morar</span>
                        </label>
                        <label>
                            <input type="checkbox" name="condicao[]" value="2" @if(in_array('2', $condicaoSelecionados)) checked @endif>
                            <span>Em construção</span>
                        </label>
                        <label>
                            <input type="checkbox" name="condicao[]" value="3" @if(in_array('3', $condicaoSelecionados)) checked @endif>
                            <span>Lançamento</span>
                        </label>
                    </div>
                    <div class="ordenar">
                        <span>ORDENAR POR:</span>
                        <label>
                            <input type="radio" name="ordem" value="asc" @if(Request::get('ordem') == 'asc') checked @endif>
                            <span>Menor valor</span>
                        </label>
                        <label>
                            <input type="radio" name="ordem" value="desc" @if(Request::get('ordem') == 'desc') checked @endif>
                            <span>Maior valor</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="codigo">
                <span>OU BUSQUE PELO CÓDIGO DO IMÓVEL:</span>
                <input type="text" name="codigo" placeholder="Código" value="{{ Request::get('codigo') }}">
            </div>
            <a href="#" class="envia-busca-avancada">APLICAR FILTROS | REALIZAR BUSCA AVANÇADA</a>
            <a href="#" class="limpar"><span>LIMPAR SELEÇÕES</span></a>
        </div>
    </div>
</div>
