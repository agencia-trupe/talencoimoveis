    <div class="footer-social">
        <div class="center">
            <span>SIGA-NOS NAS REDES SOCIAIS:</span>
            @foreach(['facebook', 'linkedin', 'instagram', 'pinterest', 'youtube'] as $s)
            @if($contato->{$s})
            <a href="{{ $contato->{$s} }}" class="social {{ $s }}">{{ $s }}</a>
            @endif
            @endforeach
            <div class="div"></div>
            <span>SAIBA MAIS NO:</span>
            <a href="{{ route('blog') }}" class="espaco">ESPAÇO <span>TALENCO</span></a>
        </div>
    </div>

    <footer>
        <div class="center">
            <div class="links-1">
                <a href="{{ route('imoveis') }}">ENCONTRE <span>SEU IMÓVEL</span></a>
                <a href="{{ route('anuncie') }}">ANUNCIE <span>SEU IMÓVEL</span></a>
                <a href="{{ route('blog') }}">ESPAÇO <span>TALENCO</span></a>
            </div>
            <div class="info">
                <div class="atendimento">
                    <div class="telefone">
                        ATENDIMENTO VIA TELEFONE
                        <span>{{ $contato->telefone }}</span>
                    </div>
                    <div class="whatsapp">
                        ATENDIMENTO VIA WHATSAPP
                        <span>{{ $contato->whatsapp }}</span>
                    </div>
                    <a href="mailto:{{ $contato->email }}" class="email">
                        ATENDIMENTO VIA
                        <span>E-MAIL</span>
                    </a>
                    <div class="chat">
                        ATENDIMENTO VIA
                        <span>CHAT</span>
                    </div>
                </div>
                <p class="endereco">{{ $contato->endereco }}</p>
            </div>
            <div class="links-2">
                <div class="wrapper">
                    <a href="{{ route('empresa') }}">&raquo; A TALENCO</a>
                    <a href="{{ route('trabalhe') }}">&raquo; TRABALHE CONOSCO</a>
                    <a href="{{ route('contato') }}">&raquo; CONTATO</a>
                </div>
            </div>
            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
