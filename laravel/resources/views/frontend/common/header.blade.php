    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                {{ $config->nome_do_site }}
                <span>CRECI: 33075-J</span>
            </a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <div id="nav-mobile" style="z-index:9999">
            @include('frontend.common.nav')
        </div>
    </header>
