<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
@if(isset($blogPost) && $blogPost->description)
    <meta name="description" content="{{ $blogPost->description }}">
@else
    <meta name="description" content="{{ $config->description }}">
@endif
@if(isset($blogPost) && $blogPost->keywords)
    <meta name="keywords" content="{{ $blogPost->keywords }}">
@else
    <meta name="keywords" content="{{ $config->keywords }}">
@endif

@if(isset($blogPost))
    <meta property="og:title" content="{{ $blogPost->titulo }} | {{ $config->title }}">
@else
    <meta property="og:title" content="{{ $config->title }}">
@endif
@if(isset($blogPost) && $blogPost->description)
    <meta property="og:description" content="{{ $blogPost->description }}">
@else
    <meta property="og:description" content="{{ $config->description }}">
@endif
    <meta property="og:site_name" content="{{ $config->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if(isset($blogPost))
    <meta property="og:image" content="{{ asset('assets/img/blog/destaque/'.$blogPost->capa) }}">
@elseif($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
@endif

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

@if(isset($blogPost))
    <title>{{ $blogPost->titulo }} | {{ $config->title }}</title>
@else
    <title>{{ $config->title }}</title>
@endif

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('vendor/fancybox/source/jquery.fancybox.css') !!}
    {!! Tools::loadCss('css/main.css') !!}
</head>
<body>
    @include('frontend.common.header')
    @yield('content')
    @include('frontend.common.footer')

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('js/vendor.main.js') !!}
    {!! Tools::loadJs('vendor/fancybox/source/jquery.fancybox.pack.js') !!}
    {!! Tools::loadJs('js/main.js') !!}

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ $config->analytics }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/59c016524854b82732ff0c55/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

    <script>
        (function () {
            var initiate = false;

            var logo;

            function targetLogo(mobile) {
                if (logo) return;

                logo = $('<div style="100%"><img width="95%" src="assets/img/layout/chat.png"><\/div>');
                logo.css({
                    position: "fixed",
                    background: "#fff",
                    right: "12px",
                    bottom: "0",
                    width: "180px",
                    height: "33px",
                    display: "none",
                    "text-align": "center",
                    "z-index": "999999999999999999999"
                });

                if (mobile) {
                    logo.css({
                        width: "100%",
                        height: "25px",
                        right: 0
                    });
                    $("img", logo).attr({
                        "width": "auto",
                        "height": "95%"
                    });
                }

                logo.appendTo("body");

                var chat;

                function isHidden(el) {
                    return String(el.css("display")).toLowerCase().indexOf("none") !== -1;
                }

                function visibilityLogo() {
                    var hide = isHidden(chat);

                    if (!hide) {
                        hide = isHidden($("iframe", chat));
                    }

                    logo.css("display", hide ? "none" : "block");

                    setTimeout(visibilityLogo, 50);
                }

                (function detectChat() {
                    chat = $("body > div[id*='-'][style], #tawkchat-container > [id*='-'][style]").filter(function () {
                        return /^[a-z\d]+-\d+$/i.test(this.id) && /^0px$|[%]$/i.test( $(this).css("width") );
                    });

                    if (!chat.length) {
                        setTimeout(detectChat, 500);
                        return;
                    }

                    visibilityLogo();
                })();
            }

            var isMobile = function() {
                var ua = window.navigator.userAgent.toLowerCase();
                var m = 0;

                if (/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i.test(ua)) {
                    ++m;
                }

                var allowUA = "," + [
                    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
                    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
                    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
                    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
                    'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
                    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
                    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
                    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
                    'wapr','webc','winw','winw','xda','xda-'
                ].join(",") + ",";

                if (allowUA.indexOf(ua.substr(0, 4)) !== -1) {
                    ++m;
                }

                if (ua.indexOf('operamini') > 0) { ++m; }
                if (ua.indexOf('opera mobi') > 0) { ++m; }
                if (ua.indexOf('windows ce') > 0) { ++m; }
                if (ua.indexOf('iemobile') > 0) { ++m; }
                if (ua.indexOf('mobile') > 0) { ++m; }

                return m > 0;
            }

            targetLogo(isMobile());
        })();
    </script>

    <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/43c2c347-1409-43b5-9945-9b44389e795e-loader.js"></script>

    <!-- Global site tag (gtag.js) - Google AdWords: 827768314 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-827768314"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-827768314');
    </script>
</body>
</html>
