<div class="blog-categorias">
    <div class="center">
        <div class="espaco-talenco">
            <span><strong>ESPAÇO</strong>TALENCO</span>
        </div>
        <nav>
            @foreach($categorias as $categoria)
            <a href="{{ route('blog', $categoria->slug) }}">{{ $categoria->titulo }}</a>
            @endforeach
        </nav>
    </div>
</div>
