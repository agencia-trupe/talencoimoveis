@extends('frontend.common.template')

@section('content')

    <div class="imoveis">
        <div class="center">
            <a href="#" onclick="window.history.go(-1); return false;" class="voltar">
                VOLTAR PARA O RESULTADO DA BUSCA
            </a>

            <div class="boxes">
                <div class="left">
                    @if(count($galeria))
                    <div class="imagens">
                        <div class="imagens-cycle-wrapper">
                            <div class="imagens-cycle">
                                @foreach($galeria as $imagem)
                                <div style="background-image:url({{ $imagem['URLArquivo'] }})"></div>
                                @endforeach
                            </div>
                            <div id="prev" style="z-index:102"></div>
                            <div id="next" style="z-index:102"></div>
                        </div>
                        <div class="carousel-wrapper">
                            <div class="imagens-carousel">
                                @foreach($galeria as $imagem)
                                <div>
                                    <div class="imagem" style="background-image:url({{ $imagem['URLArquivo'] }})"></div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(array_key_exists('Observacao', $imovel))
                    <div class="apresentacao">
                        <h3>APRESENTAÇÃO DO EMPREENDIMENTO</h3>
                        <p>{!! nl2br($imovel['Observacao']) !!}</p>
                    </div>
                    @endif
                </div>

                <div class="right">
                    <div class="titulo">
                        {{--@if(array_key_exists('NomeCondominio', $imovel) && is_string($imovel['NomeCondominio']))
                        <span>{{ $imovel['NomeCondominio'] }}</span>
                        @endif--}}
                        <span>{{ Tools::exibeTipoOferta($imovel) }}</span>
                        {{ $imovel['Bairro'] }}
                    </div>
                    <div class="info">
                        <p class="codigo">
                            CÓDIGO DO IMÓVEL: <span>{{ $imovel['CodigoImovel'] }}</span>
                        </p>
                        @if(Tools::imovelVenda($imovel))
                            <p class="sobre">
                                @if($imovel['Ocupacao'] == 'Não Informado')
                                @elseif($imovel['Ocupacao'] == 'Desocupado')
                                Pronto para morar
                                @else
                                {{ $imovel['Ocupacao'] }}
                                @endif
                                <span>{{ $imovel['TipoImovel'] }}</span>
                            </p>
                            @if(array_key_exists('PrecoVenda', $imovel) && $imovel['PrecoVenda'])
                            <p class="valor">
                                <span>R$ {{ number_format($imovel['PrecoVenda'], 2, ',', '.') }}</span>
                            </p>
                            @endif
                        @else
                            <p class="sobre"><span>{{ $imovel['TipoImovel'] }}</span></p>
                            @if(array_key_exists('PrecoLocacao', $imovel) && $imovel['PrecoLocacao'])
                            <p class="valor">
                                @if($imovel['TipoLocacao'] == '2')
                                    PACOTE DIÁRIO
                                @elseif($imovel['TipoLocacao'] == '3')
                                    PACOTE MENSAL
                                @elseif($imovel['TipoLocacao'] == '4')
                                    PACOTE ANUAL
                                @endif
                                <span>R$ {{ number_format($imovel['PrecoLocacao'], 2, ',', '.') }}</span>
                            </p>
                            @endif
                        @endif
                        @if(array_key_exists('QtdDormitorios', $imovel))
                        <p class="quartos">{{ $imovel['QtdDormitorios'] }} dormitório{{ $imovel['QtdDormitorios'] > 1 ? 's' : '' }}</p>
                        @endif
                        @if(array_key_exists('QtdSuites', $imovel))
                        <p class="suites">{{ $imovel['QtdSuites'] }} suíte{{ $imovel['QtdSuites'] > 1 ? 's' : '' }}</p>
                        @endif
                        @if(array_key_exists('QtdBanheiros', $imovel))
                        <p class="banheiros">{{ $imovel['QtdBanheiros'] }} banheiro{{ $imovel['QtdBanheiros'] > 1 ? 's' : '' }}</p>
                        @endif
                        @if(array_key_exists('QtdVagas', $imovel))
                        <p class="vagas">{{ $imovel['QtdVagas'] }} vaga{{ $imovel['QtdVagas'] > 1 ? 's' : '' }}</p>
                        @endif
                        @if(array_key_exists('AreaPrivativa', $imovel) && $imovel['AreaPrivativa'] > 0)
                        <p class="area">{{ $imovel['AreaPrivativa'] }} m²</p>
                        @endif
                        @if(count($lazer))
                        <p class="lazer">lazer</p>
                        <ul>
                            @foreach($lazer as $key => $value)
                            <li>{{ $value }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    <div class="imoveis-contato">
                        @if(session('success'))
                        <div class="contato-enviado">
                            <p>Mensagem enviada com sucesso!</p>
                            <!-- Event snippet for cadastro no site - produto conversion page -->
                            <script>
                              gtag('event', 'conversion', {
                                  'send_to': 'AW-827768314/iBVVCKu6r3kQ-vvaigM',
                                  'value': 1.0,
                                  'currency': 'BRL'
                              });
                            </script>
                        </div>
                        @else
                        <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                            <h3>QUERO SABER MAIS</h3>

                            {!! csrf_field() !!}

                            @if($errors->any())
                                <div class="erros">
                                    @foreach($errors->all() as $error)
                                    {!! $error !!}<br>
                                    @endforeach
                                </div>
                            @endif

                            {!! Form::text('nome', null, ['placeholder' => 'nome', 'required' => true]) !!}
                            {!! Form::email('email', null, ['placeholder' => 'e-mail',  'required' => true]) !!}
                            {!! Form::text('telefone', null, ['placeholder' => 'telefone']) !!}
                            {!! Form::hidden('codigo_imovel', $imovel['CodigoImovel']) !!}
                            {!! Form::textarea('mensagem', 'Olá, estou interessado neste imóvel e gostaria de receber mais informações.', ['required' => true]) !!}
                            <input type="submit" value="ENVIAR">
                        </form>
                        @endif
                    </div>
                </div>
            </div>

            @if(count($plantas))
            <div class="plantas">
                <h3>PLANTAS</h3>
                <div>
                    @foreach($plantas as $planta)
                    <a href="{{ $planta['URLArquivo'] }}#.jpg" class="fancybox" rel="plantas">
                        <img src="{{ $planta['URLArquivo'] }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
            @endif

            <div class="localizacao">
                <h3>LOCALIZAÇÃO</h3>
                <p class="endereco">
                    @if($imovel['Endereco'] && $imovel['Endereco'] != 'ENDEREÇO NÃO INFORMADO')
                        {{ $imovel['Endereco'] }} &middot;
                    @endif
                    {{ $imovel['Cidade'] }}, {{ $imovel['Estado'] }}
                </p>
                @if(array_key_exists('latitude', $imovel) && array_key_exists('longitude', $imovel))
                <div id="map"></div>
                <script>
                    function initMap() {
                        var marker = {
                            lat: {{ $imovel['latitude'] }},
                            lng: {{ $imovel['longitude'] }}
                        };
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 16,
                            center: marker
                        });
                        var marker = new google.maps.Marker({
                            position: marker,
                            map: map
                        });
                    }
                </script>
                <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASmlJAgvP1eOrtWamUqS6SZ1aJY30hIYs&callback=initMap">
                </script>
                @endif
            </div>

            <div class="compartilhamento">
                <p>
                    COMPARTILHAR ESTE ANÚNCIO:
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(request()->url()) }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" class="facebook">facebook</a>
                    <a href="https://twitter.com/share?url={{ rawurlencode(request()->url()) }}"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                       target="_blank" class="twitter">twitter</a>
                    <a href="mailto:?subject=Veja este imóvel&body={{ request()->url() }}" class="email">e-mail</a>
                </p>
                <p>
                    IMPRIMIR ESTE ANÚNCIO:
                    <a href="javascript:window.print()" class="imprimir">imprimir</a>
                </p>
            </div>
        </div>

        <div class="ofertas">
            <div class="center">
                <h3>CONFIRA OUTRAS OFERTAS</h3>
                <div>
                    @foreach($ofertas as $oferta)
                    <a href="{{ route('imoveis.show', $oferta['CodigoImovel']) }}">
                        @if(array_key_exists('Fotos', $oferta))
                        <div class="imagem" style="background-image:url({{ array_key_exists(0, $oferta['Fotos']['Foto']) ? $oferta['Fotos']['Foto'][0]['URLArquivo'] : $oferta['Fotos']['Foto']['URLArquivo'] }})">
                        </div>
                        @else
                        <div class="imagem"></div>
                        @endif
                        @if(Tools::imovelLocacao($oferta))
                            @if(array_key_exists('PrecoLocacao', $oferta) && $oferta['PrecoLocacao'])
                            <p class="info">LOCAÇÃO</p>
                            <p class="valor">
                                R$ {{ number_format($oferta['PrecoLocacao'], 2, ',', '.') }}
                            </p>
                            @endif
                        @else
                            <p class="info">VENDA</p>
                            @if(array_key_exists('PrecoVenda', $oferta) && $oferta['PrecoVenda'])
                            <p class="valor">
                                R$ {{ number_format($oferta['PrecoVenda'], 2, ',', '.') }}
                            </p>
                            @endif
                        @endif
                        <p class="bairro">{{ $oferta['Bairro'] }}</p>
                        <p class="info">
                            @if(array_key_exists('QtdDormitorios', $oferta))
                            <span>{{ $oferta['QtdDormitorios'] }} quarto{{ $oferta['QtdDormitorios'] > 1 ? 's' : '' }}</span>
                            @endif
                            @if(array_key_exists('AreaPrivativa', $oferta) && $oferta['AreaPrivativa'] > 0)
                            <span>{{ $oferta['AreaPrivativa'] }} m²</span>
                            @endif
                        </p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="center">
            <a href="#" onclick="window.history.go(-1); return false;" class="voltar">
                VOLTAR PARA O RESULTADO DA BUSCA
            </a>
        </div>
    </div>

@endsection
