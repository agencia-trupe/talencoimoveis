@extends('frontend.common.template')

@section('content')

    @include('frontend.common.busca')

    <div class="imoveis">
        <div class="center">
            @if(!count($imoveis))
            <div class="nenhum">Nenhum imóvel encontrado.</div>
            @else
            @foreach($imoveis as $imovel)
            <div class="thumb">
                @if(array_key_exists('Fotos', $imovel))
                <div class="imagem" style="background-image:url({{ array_key_exists(0, $imovel['Fotos']['Foto']) ? $imovel['Fotos']['Foto'][0]['URLArquivo'] : $imovel['Fotos']['Foto']['URLArquivo'] }})">
                    </div>
                @else
                <div class="imagem"></div>
                @endif
                <div class="info-1">
                    @if(array_key_exists('PrecoVenda', $imovel) && $imovel['PrecoVenda'])
                    <p class="valor">
                        VENDA
                        <span>R$ {{ number_format($imovel['PrecoVenda'], 2, ',', '.') }}</span>
                    </p>
                    @endif
                    @if(array_key_exists('PrecoLocacao', $imovel) && $imovel['PrecoLocacao'])
                    <p class="valor">
                        LOCAÇÃO
                        <span>R$ {{ number_format($imovel['PrecoLocacao'], 2, ',', '.') }}</span>
                    </p>
                    @endif
                    @if(array_key_exists('QtdDormitorios', $imovel))
                    <p class="quartos">{{ $imovel['QtdDormitorios'] }} dormitório{{ $imovel['QtdDormitorios'] > 1 ? 's' : '' }}</p>
                    @endif
                    @if(array_key_exists('QtdSuites', $imovel))
                    <p class="suites">{{ $imovel['QtdSuites'] }} suíte{{ $imovel['QtdSuites'] > 1 ? 's' : '' }}</p>
                    @endif
                    @if(array_key_exists('QtdBanheiros', $imovel))
                    <p class="banheiros">{{ $imovel['QtdBanheiros'] }} banheiro{{ $imovel['QtdBanheiros'] > 1 ? 's' : '' }}</p>
                    @endif
                    @if(array_key_exists('QtdVagas', $imovel))
                    <p class="vagas">{{ $imovel['QtdVagas'] }} vaga{{ $imovel['QtdVagas'] > 1 ? 's' : '' }}</p>
                    @endif
                    @if(array_key_exists('AreaPrivativa', $imovel) && $imovel['AreaPrivativa'] > 0)
                    <p class="area">{{ $imovel['AreaPrivativa'] }} m²</p>
                    @endif
                </div>
                <div class="info-2">
                    <p class="sobre">
                        {{ $imovel['TipoImovel'] }}
                        @if(Tools::imovelLocacao($imovel))
                            &middot; Locação
                        @else
                            @if($imovel['Ocupacao'] == 'Não Informado')
                            @elseif($imovel['Ocupacao'] == 'Desocupado')
                            &middot;
                            Pronto para morar
                            @else
                            &middot;
                            {{ $imovel['Ocupacao'] }}
                            @endif
                        @endif
                    </p>

                    <h3>{{ $imovel['Bairro'] }}</h3>

                    <p class="endereco">
                        @if($imovel['Endereco'] && $imovel['Endereco'] != 'ENDEREÇO NÃO INFORMADO')
                            {{ $imovel['Endereco'] }} &middot;
                        @endif
                        {{ $imovel['Cidade'] }}, {{ $imovel['Estado'] }}
                    </p>

                    <?php
                        $lazer = [];
                        foreach ($caracteristicas as $caracteristica => $texto) {
                            if (array_key_exists($caracteristica, $imovel) && $imovel[$caracteristica] == '1') {
                                $lazer[] = $texto;
                            }
                        }
                    ?>
                    <p class="caracteristicas">
                        {{ join(' - ', array_map('ucwords', $lazer)) }}
                    </p>

                    <nav>
                        <a href="{{ route('imoveis.show', $imovel['CodigoImovel']) }}">VER DETALHES</a>
                        <a href="{{ route('contato') }}">CONTATAR</a>
                    </nav>
                </div>
            </div>
            @endforeach
            @endif

            @if($imoveis->total() > $imoveis->perPage())
            <div class="paginacao">
                @if($imoveis->previousPageUrl())
                <a href="{{ $imoveis->previousPageUrl() }}" class="anterior">anterior</a>
                @endif
                <p>PÁGINA</p>
                <form action="">
                    @foreach(['oferta', 'avancada', 'tipo', 'cidade', 'bairro', 'preco-min', 'preco-max', 'area-min', 'area-max', 'dormitorios', 'suites', 'banheiros', 'vagas', 'lazer', 'condicao'] as $field)
                        @if(Request::has($field))
                        <input type="hidden" name="{{ $field }}" value="{{ Request::get($field) }}">
                        @endif
                    @endforeach
                    <input type="text" name="page" value="{{ $imoveis->currentPage() }}">
                </form>
                <p>DE {{ $imoveis->lastPage() }}</p>
                @if($imoveis->nextPageUrl())
                <a href="{{ $imoveis->nextPageUrl() }}" class="proxima">próxima</a>
                @endif
            </div>
            @endif
        </div>
    </div>

@endsection
