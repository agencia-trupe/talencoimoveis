@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="left">
                <h2>FALE CONOSCO</h2>

                @if(session('success'))
                <div class="contato-enviado">
                    <p>Mensagem enviada com sucesso!</p>
                    <!-- Event snippet for cadastro no site - produto conversion page -->
                    <script>
                      gtag('event', 'conversion', {
                          'send_to': 'AW-827768314/iBVVCKu6r3kQ-vvaigM',
                          'value': 1.0,
                          'currency': 'BRL'
                      });
                    </script>
                </div>
                @else
                <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                        </div>
                    @endif

                    {!! Form::text('nome', null, ['placeholder' => 'nome', 'required' => true]) !!}
                    {!! Form::email('email', null, ['placeholder' => 'e-mail',  'required' => true]) !!}
                    {!! Form::text('telefone', null, ['placeholder' => 'telefone']) !!}
                    {!! Form::textarea('mensagem', null, ['placeholder' => 'mensagem', 'required' => true]) !!}
                    <input type="submit" value="ENVIAR">
                </form>
                @endif

                <div class="telefone">
                    ATENDIMENTO VIA TELEFONE
                    <span>{{ $contato->telefone }}</span>
                </div>
                <div class="whatsapp">
                    ATENDIMENTO VIA WHATSAPP
                    <span>{{ $contato->whatsapp }}</span>
                </div>
            </div>
            <div class="right">
                <p>{{ $contato->endereco }}</p>
                <div class="mapa">{!! $contato->google_maps !!}</div>
            </div>
        </div>
    </div>

@endsection
