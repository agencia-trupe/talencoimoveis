@extends('frontend.common.template')

@section('content')

    <div class="anuncie">
        <div class="center">
            @if(session('success'))
            <div class="imovel-enviado">
                <p>Imóvel enviado com sucesso!</p>
                <!-- Event snippet for cadastro no site - produto conversion page -->
                <script>
                  gtag('event', 'conversion', {
                      'send_to': 'AW-827768314/iBVVCKu6r3kQ-vvaigM',
                      'value': 1.0,
                      'currency': 'BRL'
                  });
                </script>
            </div>
            @else
            <form action="{{ route('anuncie.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erros">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif

                <h2 class="proprietario">DADOS DO PROPRIETÁRIO</h2>
                {!! Form::text('nome', null, ['placeholder' => 'nome', 'required' => true]) !!}
                {!! Form::email('email', null, ['placeholder' => 'e-mail',  'required' => true]) !!}
                {!! Form::text('telefone_residencial', null, ['placeholder' => 'telefone residencial']) !!}
                {!! Form::text('telefone_celular', null, ['placeholder' => 'telefone celular']) !!}
                {!! Form::text('whatsapp', null, ['placeholder' => 'whatsapp']) !!}
                {!! Form::text('cidade_proprietario', null, ['placeholder' => 'cidade que reside']) !!}

                <h2 class="imovel">DADOS DO IMÓVEL</h2>
                {!! Form::select('tipo', ['Apartamento' => 'Apartamento', 'Casa' => 'Casa', 'Flat' => 'Flat', 'Comercial' => 'Comercial'], null, ['placeholder' => 'tipo (Selecione...)', 'required' => true]) !!}
                {!! Form::select('negocio', ['Venda' => 'Venda', 'Locação' => 'Locação'], null, ['placeholder' => 'tipo de transação (Selecione...)', 'required' => true]) !!}
                {!! Form::text('valor_imovel', null, ['placeholder' => 'valor do imóvel']) !!}
                {!! Form::text('valor_condominio', null, ['placeholder' => 'valor do condomínio']) !!}
                {!! Form::text('valor_iptu', null, ['placeholder' => 'valor do IPTU']) !!}
                {!! Form::text('area_util', null, ['placeholder' => 'área útil']) !!}
                {!! Form::text('area_total', null, ['placeholder' => 'área total']) !!}
                {!! Form::text('dormitorios', null, ['placeholder' => 'dormitórios']) !!}
                {!! Form::text('suites', null, ['placeholder' => 'suítes']) !!}
                {!! Form::text('banheiros', null, ['placeholder' => 'banheiros']) !!}
                {!! Form::text('vagas', null, ['placeholder' => 'vagas']) !!}
                {!! Form::text('tipo_vagas', null, ['placeholder' => 'tipo de vagas']) !!}
                {!! Form::text('ano_construcao', null, ['placeholder' => 'ano de construção']) !!}
                {!! Form::text('elevadores', null, ['placeholder' => 'elevadores']) !!}
                {!! Form::text('lazer', null, ['placeholder' => 'lazer']) !!}

                <h2 class="localizacao">LOCALIZAÇÃO DO IMÓVEL</h2>
                <div class="cep">
                    {!! Form::text('cep', null, ['placeholder' => 'CEP', 'required' => true]) !!}
                    <a href="#">CONSULTAR</a>
                </div>
                {!! Form::text('cidade', null, ['placeholder' => 'cidade', 'required' => true]) !!}
                {!! Form::text('bairro', null, ['placeholder' => 'bairro', 'required' => true]) !!}
                {!! Form::text('logradouro', null, ['placeholder' => 'logradouro', 'required' => true]) !!}
                {!! Form::text('numero', null, ['placeholder' => 'número', 'required' => true]) !!}
                {!! Form::text('complemento', null, ['placeholder' => 'complemento']) !!}

                <h2 class="documentacao">DOCUMENTAÇÃO</h2>
                <div class="radio">
                    <span>escritura definitiva?</span>
                    <label>
                        {!! Form::radio('escritura_definitiva', 'Sim', null, ['required' => true]) !!} SIM
                    </label>
                    <label>
                        {!! Form::radio('escritura_definitiva', 'Não', null, ['required' => true]) !!} NÃO
                    </label>
                </div>
                <div class="radio">
                    <span>está financiado?</span>
                    <label>
                        {!! Form::radio('financiado', 'Sim', null, ['required' => true]) !!} SIM
                    </label>
                    <label>
                        {!! Form::radio('financiado', 'Não', null, ['required' => true]) !!} NÃO
                    </label>
                </div>

                <h2 class="observacoes">SUAS OBSERVAÇÕES</h2>
                {!! Form::textarea('mensagem', null, ['placeholder' => 'mensagem']) !!}

                <input type="submit" value="ENVIAR">
            </form>
            @endif
        </div>
    </div>

@endsection
