@extends('frontend.common.template')

@section('content')

    <div class="empresa">
        <div class="faixa">
            <div class="center">
                <h3>NOSSO DIFERENCIAL</h3>

                <div class="video">
                    <div class="wrapper">
                        <iframe src="https://www.youtube.com/embed/{{ $empresa->codigo_video }}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="texto">
                    <p>{!! $empresa->texto_1 !!}</p>
                    <p>{!! $empresa->texto_2 !!}</p>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="unidades">
                <div class="chamada">
                    <h3>UNIDADES DE NEGÓCIO</h3>
                    <p>{{ $empresa->unidades_de_negocio }}</p>
                </div>

                <div class="unidades-lista">
                    <div>
                        <div class="imagem">
                            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_construcao) }}" alt="">
                            <span>
                                ENGENHARIA &<br>CONSTRUÇÃO
                            </span>
                        </div>
                        <div class="logo">
                            <img src="{{ asset('assets/img/layout/talenco-construcao-marca.png') }}" alt="">
                        </div>
                        <div class="texto">{{ $empresa->texto_construcao }}</div>
                    </div>
                    <div>
                        <div class="imagem">
                            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_reformas) }}" alt="">
                            <span>
                                SOLUÇÕES EM<br>REFORMAS
                            </span>
                        </div>
                        <div class="logo">
                            <img src="{{ asset('assets/img/layout/talenco-reformas-marca.png') }}" alt="">
                        </div>
                        <div class="texto">{{ $empresa->texto_reformas }}</div>
                    </div>
                    <div>
                        <div class="imagem">
                            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_negocios) }}" alt="">
                            <span>
                                NEGÓCIOS<br>IMOBILIÁRIOS
                            </span>
                        </div>
                        <div class="logo">
                            <img src="{{ asset('assets/img/layout/talenco-imobiliaria-marca.png') }}" alt="">
                        </div>
                        <div class="texto">{{ $empresa->texto_negocios }}</div>
                    </div>
                </div>
            </div>

            <div class="responsabilidade">
                <h3>RESPONSABILIDADE SOCIAL</h3>
                <p>{{ $empresa->responsabilidade_social }}</p>
            </div>
        </div>
    </div>

@endsection
