@extends('frontend.common.template')

@section('content')

    <div class="mobile-check"></div>
    <div class="banners">
        <div class="center">
            <div class="banners-cycle">
                @foreach($banners as $banner)
                <a href="{{ $banner->link }}" class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                    <div class="box">
                        @if($banner->subtitulo)
                        <p class="subtitulo">{!! $banner->subtitulo !!}</p>
                        @endif
                        @if($banner->legenda_1)
                        <p class="legenda_1">{!! $banner->legenda_1 !!}</p>
                        @endif
                        @if($banner->nome_do_empreendimento)
                        <p class="nome_do_empreendimento">{!! $banner->nome_do_empreendimento !!}</p>
                        @endif
                        @if($banner->titulo)
                        <p class="titulo">{!! $banner->titulo !!}</p>
                        @endif
                        @if($banner->area)
                        <p class="area">{!! $banner->area !!}</p>
                        @endif
                        @if($banner->valor)
                        <p class="valor">
                            {!! $banner->valor !!}
                        </p>
                        @endif
                        @if($banner->legenda_2)
                        <p class="legenda_2">{!! $banner->legenda_2 !!}</p>
                        @endif
                    </div>
                </a>
                @endforeach
                <div class="cycle-prev"></div>
                <div class="cycle-next"></div>
            </div>
        </div>
    </div>

    <div class="home-atendimento">
        <div class="center">
            <div class="telefone">
                ATENDIMENTO VIA TELEFONE
                <span>{{ $contato->telefone }}</span>
            </div>
            <div class="whatsapp">
                ATENDIMENTO VIA WHATSAPP
                <span>{{ $contato->whatsapp }}</span>
            </div>
            <a href="mailto:{{ $contato->email }}" class="email">
                ATENDIMENTO VIA
                <span>E-MAIL</span>
            </a>
            <div class="chat">
                ATENDIMENTO VIA
                <span>CHAT</span>
            </div>
        </div>
    </div>

    @include('frontend.common.busca')

    <div class="home-imoveis">
        <div class="center">
            <h1>CONFIRA ALGUNS DESTAQUES</h1>

            @foreach([
                'locacao'            => 'LOCAÇÃO',
                'prontos_para_morar' => 'PRONTOS PARA MORAR',
                'em_construcao'      => 'EM CONSTRUÇÃO',
                'lancamentos'        => 'LANÇAMENTOS'
            ] as $slug => $titulo)
                @if(count($destaques[$slug]))
                <div class="lista-destaques">
                    <h2><span>{{ $titulo }}</span></h2>
                    @foreach($destaques[$slug] as $imovel)
                    <a href="{{ route('imoveis.show', $imovel['CodigoImovel']) }}">
                        @if(array_key_exists('Fotos', $imovel))
                        <div class="imagem" style="background-image:url({{ array_key_exists(0, $imovel['Fotos']['Foto']) ? $imovel['Fotos']['Foto'][0]['URLArquivo'] : $imovel['Fotos']['Foto']['URLArquivo'] }})">
                        </div>
                        @else
                        <div class="imagem"></div>
                        @endif
                        <div class="box">
                            <p>
                                {{ $imovel['TipoImovel'] }}<br>
                                {{ Tools::exibeTipoOferta($imovel) }}
                                @if(array_key_exists('AreaPrivativa', $imovel)) &middot; {{ $imovel['AreaPrivativa'] }}m² @endif
                            </p>
                            <h3>{{ $imovel['Bairro'] }}</h3>
                            @if(array_key_exists('PrecoVenda', $imovel) && $imovel['PrecoVenda'])
                            <p class="valor">
                                <span>R$ {{ number_format($imovel['PrecoVenda'], 2, ',', '.') }}</span>
                            </p>
                            @endif
                            @if(array_key_exists('PrecoLocacao', $imovel) && $imovel['PrecoLocacao'])
                            <p class="valor">
                                <span>R$ {{ number_format($imovel['PrecoLocacao'], 2, ',', '.') }}</span>
                            </p>
                            @endif
                        </div>
                    </a>
                    @endforeach
                </div>
                @endif
            @endforeach
        </div>
    </div>

    <div class="home-blog">
        <div class="center">
            <div class="espaco-talenco">
                <span><strong>ESPAÇO</strong>TALENCO</span>
            </div>

            <div class="posts">
                @foreach($blog as $post)
                <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/blog/mais/'.$post->capa) }}" alt="">
                        <span>{{ Tools::formataData($post->data) }}</span>
                    </div>

                    <h3>{{ $post->titulo }}</h3>
                    <p>{{ Tools::chamadaPost($post->texto) }}</p>

                    <span class="mais">LER MAIS</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
