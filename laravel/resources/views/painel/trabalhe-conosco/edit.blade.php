@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Trabalhe Conosco</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.trabalhe-conosco.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.trabalhe-conosco.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
