@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (1400 x 500px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::textarea('subtitulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('legenda_1', 'Legenda 1') !!}
    {!! Form::text('legenda_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_do_empreendimento', 'Nome do empreendimento') !!}
    {!! Form::text('nome_do_empreendimento', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::textarea('titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('area', 'Área') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor', 'Valor') !!}
    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('legenda_2', 'Legenda 2') !!}
    {!! Form::text('legenda_2', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
