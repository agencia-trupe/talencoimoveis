@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('codigo_video', 'Código Vídeo') !!}
    {!! Form::text('codigo_video', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1', 'Texto 1') !!}
            {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'empresa1']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_2', 'Texto 2') !!}
            {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'empresa2']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('unidades_de_negocio', 'Unidades de Negócio') !!}
    {!! Form::textarea('unidades_de_negocio', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_construcao', 'Engenharia e Construção') !!}
            <div class="well">
            @if($submitText == 'Alterar')
                <img src="{{ url('assets/img/empresa/'.$registro->imagem_construcao) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
                {!! Form::file('imagem_construcao', ['class' => 'form-control']) !!}
            </div>
            {!! Form::textarea('texto_construcao', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_reformas', 'Soluções em Reformas') !!}
            <div class="well">
            @if($submitText == 'Alterar')
                <img src="{{ url('assets/img/empresa/'.$registro->imagem_reformas) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
                {!! Form::file('imagem_reformas', ['class' => 'form-control']) !!}
            </div>
            {!! Form::textarea('texto_reformas', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_negocios', 'Negócios Imobiliários') !!}
            <div class="well">
            @if($submitText == 'Alterar')
                <img src="{{ url('assets/img/empresa/'.$registro->imagem_negocios) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
                {!! Form::file('imagem_negocios', ['class' => 'form-control']) !!}
            </div>
            {!! Form::textarea('texto_negocios', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('responsabilidade_social', 'Responsabilidade Social') !!}
    {!! Form::textarea('responsabilidade_social', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
