@include('painel.common.flash')

<div class="alert alert-info">
    Insira os códigos dos imóveis em destaque. Campos em branco ou inválidos serão exibidos randomicamente.
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('prontos_para_morar_1', 'Prontos para morar') !!}
            {!! Form::text('prontos_para_morar_1', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::text('prontos_para_morar_2', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::text('prontos_para_morar_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('em_construcao_1', 'Em construção') !!}
            {!! Form::text('em_construcao_1', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::text('em_construcao_2', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::text('em_construcao_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('lancamentos_1', 'Lançamentos') !!}
            {!! Form::text('lancamentos_1', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::text('lancamentos_2', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::text('lancamentos_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
