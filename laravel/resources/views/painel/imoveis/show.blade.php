@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Imóveis</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <hr>

    <h3 style="margin-bottom:1em">Dados do proprietário</h3>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->email }}
        </div>
    </div>

@if($contato->telefone_residencial)
    <div class="form-group">
        <label>Telefone residencial</label>
        <div class="well">{{ $contato->telefone_residencial }}</div>
    </div>
@endif

@if($contato->telefone_celular)
    <div class="form-group">
        <label>Telefone celular</label>
        <div class="well">{{ $contato->telefone_celular }}</div>
    </div>
@endif

@if($contato->whatsapp)
    <div class="form-group">
        <label>WhatsApp</label>
        <div class="well">{{ $contato->whatsapp }}</div>
    </div>
@endif

@if($contato->cidade_proprietario)
    <div class="form-group">
        <label>Cidade que reside</label>
        <div class="well">{{ $contato->cidade_proprietario }}</div>
    </div>
@endif

    <hr>

    <h3 style="margin-bottom:1em">Localização do imóvel</h3>

    <div class="form-group">
        <label>CEP</label>
        <div class="well">{{ $contato->cep }}</div>
    </div>

    <div class="form-group">
        <label>Cidade</label>
        <div class="well">{{ $contato->cidade }}</div>
    </div>

    <div class="form-group">
        <label>Bairro</label>
        <div class="well">{{ $contato->bairro }}</div>
    </div>

    <div class="form-group">
        <label>Logradouro</label>
        <div class="well">{{ $contato->logradouro }}</div>
    </div>

    <div class="form-group">
        <label>Número</label>
        <div class="well">{{ $contato->numero }}</div>
    </div>

@if($contato->complemento)
    <div class="form-group">
        <label>Complemento</label>
        <div class="well">{{ $contato->complemento }}</div>
    </div>
@endif

    <hr>

    <h3 style="margin-bottom:1em">Dados do imóvel</h3>

    <div class="form-group">
        <label>Tipo</label>
        <div class="well">{{ $contato->tipo }}</div>
    </div>

    <div class="form-group">
        <label>Tipo de Transação</label>
        <div class="well">{{ $contato->negocio }}</div>
    </div>

@if($contato->valor_imovel)
    <div class="form-group">
        <label>Valor do imóvel</label>
        <div class="well">{{ $contato->valor_imovel }}</div>
    </div>
@endif

@if($contato->valor_condominio)
    <div class="form-group">
        <label>Valor do condomínio</label>
        <div class="well">{{ $contato->valor_condominio }}</div>
    </div>
@endif

@if($contato->valor_iptu)
    <div class="form-group">
        <label>Valor do IPTU</label>
        <div class="well">{{ $contato->valor_iptu }}</div>
    </div>
@endif

@if($contato->area_util)
    <div class="form-group">
        <label>Área útil</label>
        <div class="well">{{ $contato->area_util }}</div>
    </div>
@endif

@if($contato->area_total)
    <div class="form-group">
        <label>Área total</label>
        <div class="well">{{ $contato->area_total }}</div>
    </div>
@endif

@if($contato->dormitorios)
    <div class="form-group">
        <label>Dormitórios</label>
        <div class="well">{{ $contato->dormitorios }}</div>
    </div>
@endif

@if($contato->suites)
    <div class="form-group">
        <label>Suítes</label>
        <div class="well">{{ $contato->suites }}</div>
    </div>
@endif

@if($contato->banheiros)
    <div class="form-group">
        <label>Banheiros</label>
        <div class="well">{{ $contato->banheiros }}</div>
    </div>
@endif

@if($contato->vagas)
    <div class="form-group">
        <label>Vagas</label>
        <div class="well">{{ $contato->vagas }}</div>
    </div>
@endif

@if($contato->tipo_vagas)
    <div class="form-group">
        <label>Tipo de vagas</label>
        <div class="well">{{ $contato->tipo_vagas }}</div>
    </div>
@endif

@if($contato->ano_construcao)
    <div class="form-group">
        <label>Ano de construção</label>
        <div class="well">{{ $contato->ano_construcao }}</div>
    </div>
@endif

@if($contato->elevadores)
    <div class="form-group">
        <label>Elevadores</label>
        <div class="well">{{ $contato->elevadores }}</div>
    </div>
@endif

@if($contato->lazer)
    <div class="form-group">
        <label>Lazer</label>
        <div class="well">{{ $contato->lazer }}</div>
    </div>
@endif

    <hr>

    <h3 style="margin-bottom:1em">Documentação</h3>

    <div class="form-group">
        <label>Escritura definitiva</label>
        <div class="well">{{ $contato->escritura_definitiva }}</div>
    </div>

    <div class="form-group">
        <label>Financiado</label>
        <div class="well">{{ $contato->financiado }}</div>
    </div>

@if($contato->mensagem)
    <hr>

    <h3 style="margin-bottom:1em">Observações</h3>

    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $contato->mensagem }}</div>
    </div>
@endif

    <a href="{{ route('painel.imoveis.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
