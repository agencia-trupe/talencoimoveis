@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('blog_categoria_id', 'Categoria') !!}
    {!! Form::select('blog_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('data', 'Data de publicação (posts com datas posteriores ao dia atual serão agendados)') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Meta-descrição') !!}
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('keywords', 'Palavras-chave (separadas por vírgula)') !!}
            {!! Form::text('keywords', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/blog/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.blog.index') }}" class="btn btn-default btn-voltar">Voltar</a>
