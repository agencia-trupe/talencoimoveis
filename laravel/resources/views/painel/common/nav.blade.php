<ul class="nav navbar-nav">
@if(\Auth::user()->acesso == 1)
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    {{--<li @if(str_is('painel.destaques*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.destaques.index') }}">Destaques</a>
    </li>--}}
    <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.empresa.index') }}">Empresa</a>
    </li>
    <li @if(str_is('painel.imoveis*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.imoveis.index') }}">
            Imóveis
            @if($imoveisNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $imoveisNaoLidos }}</span>
            @endif
        </a>
    </li>
@endif
    <li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.blog.index') }}">Blog</a>
    </li>
@if(\Auth::user()->acesso == 1)
	<li @if(str_is('painel.trabalhe-conosco*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.trabalhe-conosco.index') }}">Trabalhe Conosco</a>
	</li>
    <li @if(str_is('painel.curriculos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.curriculos.index') }}">
            Currículos
            @if($curriculosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $curriculosNaoLidos }}</span>
            @endif
        </a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName()) || str_is('painel.newsletter*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li class="divider"></li>
            <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>
@endif
</ul>
