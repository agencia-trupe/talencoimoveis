<!DOCTYPE html>
<html>
<head>
    <title>[IMÓVEL] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <h2 style='font-weight:bold;font-size:20px;font-family:Verdana;'>DADOS DO PROPRIETÁRIO</h2>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
@if($telefone_residencial)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone residencial:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone_residencial }}</span><br>
@endif
@if($telefone_celular)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone celular:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone_celular }}</span><br>
@endif
@if($whatsapp)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>WhatsApp:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $whatsapp }}</span><br>
@endif
@if($cidade_proprietario)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cidade que reside:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cidade_proprietario }}</span><br>
@endif

    <hr>

    <h2 style='font-weight:bold;font-size:20px;font-family:Verdana;'>LOCALIZAÇÃO DO IMÓVEL</h2>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CEP:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cep }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cidade:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cidade }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Bairro:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $bairro }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Logradouro:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $logradouro }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Número:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $numero }}</span><br>
@if($complemento)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Complemento:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $complemento }}</span><br>
@endif

    <hr>

    <h2 style='font-weight:bold;font-size:20px;font-family:Verdana;'>DADOS DO IMÓVEL</h2>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $tipo }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de Transação:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $negocio }}</span><br>
@if($valor_imovel)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Valor do imóvel:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $valor_imovel }}</span><br>
@endif
@if($valor_condominio)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Valor do condomínio:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $valor_condominio }}</span><br>
@endif
@if($valor_iptu)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Valor do IPTU:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $valor_iptu }}</span><br>
@endif
@if($area_util)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Área útil:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $area_util }}</span><br>
@endif
@if($area_total)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Área total:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $area_total }}</span><br>
@endif
@if($dormitorios)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Dormitórios:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $dormitorios }}</span><br>
@endif
@if($suites)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Suítes:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $suites }}</span><br>
@endif
@if($banheiros)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Banheiros:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $banheiros }}</span><br>
@endif
@if($vagas)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Vagas:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $vagas }}</span><br>
@endif
@if($tipo_vagas)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de vagas:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $tipo_vagas }}</span><br>
@endif
@if($ano_construcao)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Ano de construção:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $ano_construcao }}</span><br>
@endif
@if($elevadores)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Elevadores:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $elevadores }}</span><br>
@endif
@if($lazer)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Lazer:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $lazer }}</span><br>
@endif

    <hr>

    <h2 style='font-weight:bold;font-size:20px;font-family:Verdana;'>DOCUMENTAÇÃO</h2>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Escritura definitiva:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $escritura_definitiva }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Está financiado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $financiado }}</span><br>

@if($mensagem)
    <hr>
    <h2 style='font-weight:bold;font-size:20px;font-family:Verdana;'>OBSERVAÇÕES</h2>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span><br>
@endif
</body>
</html>
